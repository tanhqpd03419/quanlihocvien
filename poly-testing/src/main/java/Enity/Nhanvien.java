/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

/**
 *
 * @author Administrator
 */
public class Nhanvien {
    String MaNV;
    String Matkhau;
    boolean VaiTro;
    String hoten;

    public Nhanvien() {
    }

    public Nhanvien(String MaNV, String Matkhau, boolean VaiTro, String hoten) {
        this.MaNV = MaNV;
        this.Matkhau = Matkhau;
        this.VaiTro = VaiTro;
        this.hoten = hoten;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getMatkhau() {
        return Matkhau;
    }

    public void setMatkhau(String Matkhau) {
        this.Matkhau = Matkhau;
    }

    public boolean isVaiTro() {
        return VaiTro;
    }

    public void setVaiTro(boolean VaiTro) {
        this.VaiTro = VaiTro;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }


    
    
}
