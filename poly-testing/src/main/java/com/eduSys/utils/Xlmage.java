/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.utils;

import Enity.Nhanvien;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class Xlmage {
    public static Image getAppIcon() {
        ImageIcon img = new ImageIcon(".../Hinh/logo.png");
        return img.getImage();
//        URL url = XImage.class.getResource("src\\icons\\logo.png");
//         return new ImageIcon(url).getImage();
    }

    public static void save(File src) {
        File dst = new File("images", src.getName());
        if (!dst.getParentFile().exists()) {
            dst.getParentFile().mkdirs();
        }
        try {
            Path from = Paths.get(src.getAbsolutePath());
            Path to = Paths.get(dst.getAbsolutePath());
            Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static ImageIcon read(String fileName) {
        File path = new File("images", fileName);
        return new ImageIcon(path.getAbsolutePath());
    }
////    public  static  final Image getAppicon(){
////      URL url=Xlmage.class.getResource("/Hinh/fpt.png/");
////      return new ImageIcon(url).getImage();
////    }
////   public static boolean save(File src){
////        File dst =new File("logos",src.getName());
////        if(!dst.getParentFile().exists()){
////             dst.getParentFile().mkdirs();
////            try {
////               
////                Path from =Paths.get(src.getAbsolutePath());
////                Path to = Paths.get(dst.getAbsolutePath());
////                Files.copy(from,to,StandardCopyOption.REPLACE_EXISTING);
////                return true;
////            } catch (Exception ex) {
////                throw new RuntimeException(ex);
////                
////            }
////        }
////        return false;
////   }
////    public static ImageIcon read(String fileName){
////        File path = new File("logos",fileName);
////        return new ImageIcon(path.getAbsolutePath());
////    }
////    public static Nhanvien USER = null;
////}
//        public static final Image APP_ICON;
//
//    static {
//        String file = "/Hinh/fpt.png/";
//        APP_ICON = new ImageIcon(Xlmage.class.getResource(file)).getImage();
//    }
//
//    public static boolean saveLogo(File file) {
//        File dir = new File("logos");
//        if (!dir.exists()) {
//            dir.mkdirs();
//        }
//        File newFile = new File(dir, file.getName());
//        try {
//            Path source = Paths.get(file.getAbsolutePath());
//            Path destination = Paths.get(newFile.getAbsolutePath());
//            Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
//
//    /**
//     * * Đọc hình ảnh logo chuyên đề * @param fileName là tên file logo *
//     * @return ảnh đọc được
//     */
//    public static ImageIcon readLogo(String fileName) {
//        File path = new File("logos", fileName);
//        return new ImageIcon(path.getAbsolutePath());
//    }
    /**
     * * Đối tượng này chứa thông tin người sử dụng sau khi đăng nhập
     */
//    public static Nhanvien USER = null;

    /**
     * * Xóa thông tin của người sử dụng khi có yêu cầu đăng xuất
     */
//    public static void logoff() {
//        Xlmage.USER = null;
//    }

    /**
     * * Kiểm tra xem đăng nhập hay chưa * @return đăng nhập hay chưa
     */
//    public static boolean authenticated() {
//        return Xlmage.USER != null;
//    }
}