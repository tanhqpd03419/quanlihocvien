/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class JdbcHelper {

    static String url = "jdbc:sqlserver://localhost:1433;databaseName=EduSys";
    static String user = "sa";
    static String pass = "123456";

    static {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        } catch (Exception ex) {
            throw new RuntimeException(ex);
            
        }

    }

    public static PreparedStatement getStmt(String sql, Object... args) throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, pass);
        PreparedStatement stmt;
        if (sql.trim().startsWith("{")) {
            stmt = conn.prepareCall(sql);

        } else {
            stmt = conn.prepareStatement(sql);
        }
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);

        }
        return stmt;
    }

    public static ResultSet query(String sql, Object... args) throws SQLException {
        PreparedStatement stmt = JdbcHelper.getStmt(sql, args);
        return stmt.executeQuery();
    }

    public static Object value(String sql, Object... args) {
        try {
            ResultSet rs = JdbcHelper.query(sql, args);
            if (rs.next()) {
                return rs.getObject(0);
            }
            rs.getStatement().getConnection().close();
            return null;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }
//    public static int update(String sql,Object...args){
//             try {
//                 PreparedStatement stmt =JdbcHelper.getStmt(sql, args);
//                 try {
//                   return stmt.executeUpdate();
//                 } 
//                 finally{
//                     stmt.getConnection().close();
//                 }
//             } catch (SQLException ex) {
//                 ex.printStackTrace();
//               throw new RuntimeException(ex);
//             }
//    }

    public static void update(String sql, Object... args) {
        try {

            PreparedStatement stmt = JdbcHelper.getStmt(sql, args);
            try {
                stmt.executeUpdate();
            } finally {
                stmt.getConnection().close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
//         
//     }
//     Connection connection =null;
//        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//
//            connection=DriverManager.getConnection(url , user, pass);
//        } catch (ClassNotFoundException ex) {
//           throw new RuntimeException(ex);
//        }
//        return connection;
//    }
// public static PreparedStatement preparedStatement (String sql ,) throws SQLException{
//       String url="jdbc:sqlserver://localhost:1433;databaseName=Esysedu";
//            String user="sa";
//            String pass="123456";
//   Connection connection=DriverManager.getConnection(url , user, pass);
//     PreparedStatement pstmt =null;
//     if(sql.trim().startsWith("{")){
//         pstmt =connection.prepareStatement(sql);
//         
//     }
//     else{
//         pstmt=connection.prepareStatement(sql);
//     }
//     for (int i = 0; i <nv.; i++) {
//        
//         
//     }
//     
//     
// }

}
