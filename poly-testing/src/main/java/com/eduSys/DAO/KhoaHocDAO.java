/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import Enity.KhoaHoc;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class KhoaHocDAO extends EduSysDAO<KhoaHoc, String> {

    @Override
    public void insert(KhoaHoc enity) {
        String sql = "INSERT INTO KhoaHoc(MaCD ,HocPhi, ThoiLuong ,NgayKG, GhiChu, MaNV ,NgayTao)VALUES (?,?,?,?,?,?,?)";
        JdbcHelper.update(sql,
                enity.getMaCD(),
                enity.getHocPhi(), enity.getThoiLuong(),
                enity.getNgayKG(), enity.getGhiChu(), enity.getMaNV(), enity.getNgayTao());
    }

    @Override
    public void update(KhoaHoc enity) {
        String sql ="UPDATE KhoaHoc SET MaCD=?, HocPhi=?, ThoiLuong=?, NgayKG=?, GhiChu=?, MaNV=? WHERE MaKH=?";
        JdbcHelper.update(sql,
                enity.getMaCD(),
                enity.getHocPhi(), enity.getThoiLuong(),
                enity.getNgayKG(), enity.getGhiChu(), enity.getMaNV(), enity.getMaKH());

    }

    @Override
    public void delete(String makh) {
        String sql = "DELETE FROM KhoaHoc WHERE MaKH=?";
        JdbcHelper.update(sql,makh);
    }

    @Override
    public List<KhoaHoc> selectAll() {
        String sql = "select*from KhoaHoc";
        return this.selectBySql(sql);
    }

    @Override
    public KhoaHoc selectById(String makh) {
        String sql = "select*from KhoaHoc where MaKH=?";
        List<KhoaHoc> list = selectBySql(sql, makh);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    protected List<KhoaHoc> selectBySql(String sql, Object... args) {
        List<KhoaHoc> list = new ArrayList<KhoaHoc>();
        try {
            ResultSet rs = JdbcHelper.query(sql, args);
            while (rs.next()) {
                KhoaHoc entity = new KhoaHoc();
                entity.setMaKH(rs.getInt("MaKH"));
                entity.setMaCD(rs.getString("MaCD"));
                entity.setHocPhi(rs.getFloat("HocPhi"));
                entity.setThoiLuong(rs.getInt("ThoiLuong"));
                entity.setNgayKG(rs.getDate("NgayKG"));
                entity.setGhiChu(rs.getString("GhiChu"));
                entity.setMaNV(rs.getString("MaNV"));
                entity.setNgayTao(rs.getDate("NgayTao"));
                list.add(entity);
//                System.out.println("entity: "+ entity.getMaKH() );
            }
            rs.getStatement().getConnection().close();

            return list;

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);

        }
    }

    public List<KhoaHoc> selectByChuyenDe(String macd) {
        String sql = "SELECT * FROM KhoaHoc WHERE MaCD=?";
        return this.selectBySql(sql, macd);
    }

    public List<Integer> selectYears() {

        String sql = "SELECT DISTINCT year(NgayKG) FROM KhoaHoc ORDER BY Year DESC";
        List<Integer> list = new ArrayList<>();
        try {
            ResultSet rs = JdbcHelper.query(sql);
            while (rs.next()) {
                list.add(rs.getInt(1));
            }
            rs.getStatement().getConnection().close();
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }

    }
}
