/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import Enity.HocVien;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class HocVienDAO extends EduSysDAO<HocVien, String>
{

    @Override
    public void insert(HocVien enity) {
       String sql ="INSERT INTO HocVien(MaKH,MaNH,Diem) values (?,?,?)";
       JdbcHelper.update(sql, 
              
               enity.getMaKH(),
               enity.getMaNH(),
               enity.getDiem()
       );
    }

    @Override
    public void update(HocVien enity) {
        String sql ="UPDATE HocVien SET "
                + "MaKH=?, MaNH=?,"
                + " Diem= ? WHERE MaHV=?";
        JdbcHelper.update(sql,
                enity.getMaKH(),enity.getMaNH(),
                enity.getDiem(),enity.getMaHV());
       
    }

   //
    public void delete(String mahv) {
        String sql ="DELETE FROM HocVien WHERE MaHV=?";
        JdbcHelper.update(sql, mahv);
    }

    @Override
    public List<HocVien> selectAll() {
       String sql ="SELECT*FROM HocVien";
       return this.selectBySql(sql);
    }

    
    public HocVien selectById(Integer mahv) {
          String sql ="SELECT * FROM HocVien WHERE MaHV=?";
        List<HocVien>list =this.selectBySql(sql,mahv);
        if(list.isEmpty()){
            return null;
        }
        return list.get(0);
    }

    @Override
    protected List<HocVien> selectBySql(String sql, Object... args) {
       List<HocVien> list =new ArrayList<HocVien>();
           try{
            ResultSet rs = JdbcHelper.query(sql, args);
               while (rs.next()) { 
        HocVien entity = new HocVien();
        entity.setMaHV(rs.getInt("MaHV"));
        entity.setMaKH(rs.getInt("MaKH"));
        entity.setMaNH(rs.getString("MaNH"));
        entity.setDiem(rs.getFloat("Diem"));
         list.add(entity);
               }
               rs.getStatement().getConnection().close();
         
               return list;
        }
        catch (SQLException ex) {
            throw new RuntimeException(ex);
                    
        }
    }
      public List<HocVien>selectByKhoaHoc(int maKH){
      String sql ="SELECT*FROM HocVien WHERE MaKH=?";
      return this.selectBySql(sql,maKH);
  }

    @Override
    public HocVien selectById(String key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
