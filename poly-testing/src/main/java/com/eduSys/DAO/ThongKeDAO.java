/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class ThongKeDAO {
   private List<Object[]>getListofArray(String sql,String[]cols,Object...args){
       try {
           List<Object[]> list =new ArrayList<>();
           ResultSet rs =JdbcHelper.query(sql, args);
           while (rs.next()) {               
               Object[]vals =new Object[cols.length];
               for (int i = 0; i < cols.length; i++) {
                   vals[i]=rs.getObject(cols[i]);
                   
               }
               list.add(vals);
           }
           rs.getStatement().getConnection().close();
           return list;
       } catch (SQLException ex) {
           throw  new RuntimeException(ex);
       }
       
   }
   
   public List<Object[]>getBangDiem(Integer makh){
     String sql ="{CALL sp_BangDiem(?)}";
     String[] cols ={"MaNH","HoTen","Diem"};
     return this.getListofArray(sql, cols,makh);

   }
      public List<Object[]>getLuongNguoiHoc(){
     String sql ="{CALL sp_LuongNguoiHoc}";
     String[] cols ={"Nam","SoLuong","DauTien","CuoiCung"};
     return this.getListofArray(sql, cols);
   }
         public List<Object[]>getDiemChuyenDe(){
     String sql ="{CALL sp_DiemChuyenDe1}";
     String[] cols ={"ChuyenDe","SoHV","ThapNhat","CaoNhat","TrungBinh"};
     return this.getListofArray(sql, cols);
   }
            public List<Object[]>getDoanhThu(int nam){
     String sql ="{CALL sp_DoanhThu(?)}";
     String[] cols ={"ChuyenDe","SoKH","SoHV","DoanhThu","ThapNhat","CaoNhat","TrungBinh"};
     return this.getListofArray(sql,cols, nam);
   }

}
