/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import Enity.HocVien;
import com.eduSys.utils.JdbcHelper;
import Enity.NguoiHoc;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class NguoiHocDAO extends EduSysDAO<NguoiHoc, String> {

    @Override
    public void insert(NguoiHoc enity) {
        String sql = "INSERT INTO NguoiHoc (MaNH,HoTen,NgaySinh,GioiTinh,DienThoai,Email,GhiChu,MaNV,NgayDK) values (?,?,?,?,?,?,?,?,?)";
        JdbcHelper.update(sql,
                enity.getMaNH(), enity.getHoTen(), enity.getNgaySinh(),
                enity.isGioiTinh(), enity.getDienThoai(), enity.getEmail(), enity.getGhiChu(),
                enity.getMaNV(), enity.getNgayDK());
    }

    @Override
    public void update(NguoiHoc enity) {
        String sql = "UPDATE NguoiHoc SET HoTen=?, NgaySinh=?, GioiTinh=?, DienThoai=?, Email=?, GhiChu=?, MaNV=?,NgayDK=? WHERE MaNH=?";
        JdbcHelper.update(sql,
                enity.getHoTen(), enity.getNgaySinh(),
                enity.isGioiTinh(), enity.getDienThoai(), enity.getEmail(), enity.getGhiChu(),
                enity.getMaNV(), enity.getNgayDK(), enity.getMaNH());
    }

    @Override
    public void delete(String manh) {
        String sql = "DELETE FROM NguoiHoc where MaNH=? ";
        JdbcHelper.update(sql, manh);

    }

    @Override
    public List<NguoiHoc> selectAll() {
        String sql = "Select*from NguoiHoc";
        return selectBySql(sql);
    }

    @Override
    public NguoiHoc selectById(String manh) {
        String sql = "select*from NguoiHoc where Manh=?";
        List<NguoiHoc> list = selectBySql(sql, manh);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    protected List<NguoiHoc> selectBySql(String sql, Object... args) {
        List<NguoiHoc> list = new ArrayList<NguoiHoc>();
        try {
            ResultSet rs = JdbcHelper.query(sql, args);
            
            while (rs.next()) {
                NguoiHoc entity = new NguoiHoc();
                entity.setMaNH(rs.getString("MaNH"));
                entity.setHoTen(rs.getString("HoTen"));
                entity.setNgaySinh(rs.getDate("NgaySinh"));
                entity.setGioiTinh(rs.getBoolean("GioiTinh"));
                entity.setDienThoai(rs.getInt("DienThoai"));
                entity.setEmail(rs.getString("Email"));
                entity.setGhiChu(rs.getString("GhiChu"));
                entity.setMaNV(rs.getString("MaNV"));
                entity.setNgayDK(rs.getDate("NgayDK"));
                
                list.add(entity);
            }
            rs.getStatement().getConnection().close();

            return list;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);

        }
    }

    public List<NguoiHoc> SelectByKeyword(String keyword) {
        String sql = "SELECT * FROM NguoiHoc WHERE HoTen LIKE ?";
        return selectBySql(sql, "%" + keyword + "%");
    }

    public List<NguoiHoc> selectNotlnCourse(int makh, String Keyword) {
        String sql = "SELECT*FROM NguoiHoc "
                + " WHERE HoTen LIKE ? AND "
                + " MaNH NOT IN(SELECT MaNH FROM HocVien WHERE MaKH=?)";
        return this.selectBySql(sql, "%" + Keyword + "%", makh);

    }
//    public List<NguoiHoc> selectByCourse(Integer makh) {
//        String sql = "SELECT * FROM NguoiHoc WHERE MaNH NOT IN (SELECT MaNH FROM HocVien WHERE MaKH=?)";
//        return selectBySql(sql, makh);
//    }
}
//public class NguoiHocDAO {
//
//    public void insert(NguoiHoc model) {
//        String sql = "INSERT INTO NguoiHoc (MaNH, HoTen, NgaySinh, GioiTinh, DienThoai, Email, GhiChu, MaNV,NgayDK) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)";
//       
//        JdbcHelper.update(sql, model.getMaNH(), model.getHoTen(), model.getNgaySinh(), model.isGioiTinh(), model.getDienThoai(), model.getEmail(), model.getGhiChu(), model.getMaNV(),model.getNgayDK());
//    }
//
//    public void update(NguoiHoc model) {
//        String sql = "UPDATE NguoiHoc SET HoTen=?, NgaySinh=?, GioiTinh=?, DienThoai=?, Email=?, GhiChu=?, MaNV=? WHERE MaNH=?";
//        JdbcHelper.update(sql, model.getHoTen(), model.getNgaySinh(), model.isGioiTinh(), model.getDienThoai(), model.getEmail(), model.getGhiChu(), model.getMaNV(), model.getMaNH());
//    }
//
//    public void delete(String id) {
//        String sql = "DELETE FROM NguoiHoc WHERE MaNH=?";
//        JdbcHelper.update(sql, id);
//    }
//
//    public List<NguoiHoc> select() {
//        String sql = "SELECT * FROM NguoiHoc";
//        return select(sql);
//    }
//
//    public List<NguoiHoc> selectByKeyword(String keyword) {
//        String sql = "SELECT * FROM NguoiHoc WHERE HoTen LIKE ?";
//        return select(sql, "%" + keyword + "%");
//    }
//
//    public List<NguoiHoc> selectByCourse(Integer makh) {
//        String sql = "SELECT * FROM NguoiHoc WHERE MaNH NOT IN (SELECT MaNH FROM HocVien WHERE MaKH=?)";
//        return select(sql, makh);
//    }
//
//    public NguoiHoc findById(String manh) {
//        String sql = "SELECT * FROM NguoiHoc WHERE MaNH=?";
//        List<NguoiHoc> list = select(sql, manh);
//        return list.size() > 0 ? list.get(0) : null;
//    }
//
//    private List<NguoiHoc> select(String sql, Object... args) {
//        List<NguoiHoc> list = new ArrayList<>();
//        try {
//            ResultSet rs = null;
//            try {
//                rs = JdbcHelper.query(sql, args);
//                while (rs.next()) {
//                    NguoiHoc model = readFromResultSet(rs);
//                    list.add(model);
//                }
//            } finally {
//
//                rs.getStatement().getConnection().close();
//            }
//        } catch (SQLException ex) {
//            throw new RuntimeException(ex);
//        }
//        return list;
//    }
//
//    private NguoiHoc readFromResultSet(ResultSet rs) throws SQLException {
//        NguoiHoc model = new NguoiHoc();
//        model.setMaNH(rs.getString("MaNH"));
//        model.setHoTen(rs.getString("HoTen"));
//        model.setNgaySinh(rs.getDate("NgaySinh"));
//        model.setGioiTinh(rs.getBoolean("GioiTinh"));
//        model.setDienThoai(rs.getInt("DienThoai"));
//        model.setEmail(rs.getString("Email"));
//        model.setGhiChu(rs.getString("GhiChu"));
//        model.setMaNV(rs.getString("MaNV"));
//        model.setNgayDK(rs.getDate("NgayDK"));
//        return model;
//    }
//}