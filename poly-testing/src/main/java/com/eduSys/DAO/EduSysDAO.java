/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import java.util.List;

/**
 *
 * @author Administrator
 */
public abstract class EduSysDAO<E,K> {
    abstract public void insert(E enity);
    abstract public void update(E enity);
    abstract public void delete(K key);
    abstract public List<E>selectAll();
    abstract public E selectById(K key);
    abstract protected List<E> selectBySql(String sql,Object...args);
    
}
