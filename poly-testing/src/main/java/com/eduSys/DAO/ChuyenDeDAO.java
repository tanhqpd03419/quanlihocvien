/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import Enity.ChuyenDe;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ChuyenDeDAO extends EduSysDAO<ChuyenDe, String>{

    @Override
    public void insert(ChuyenDe enity) {
       String sql="INSERT INTO ChuyenDe(MaCD,TenCD,HocPhi,ThoiLuong,Hinh,MoTa) VALUES (?,?,?,?,?,?)";
             JdbcHelper.update(sql,
             enity.getMaCD(),enity.getTenCD(),
             enity.getHocPhi(),enity.getThoiLuong(),
             enity.getHinh(),enity.getMoTa());
    }

    @Override
    public void update(ChuyenDe enity) {
        String sql ="Update ChuyenDe set TenCD=?, HocPhi=?, ThoiLuong=? ,MoTa=? ,Hinh=? where MaCD=?";
        JdbcHelper.update(sql,
             enity.getTenCD(),
             enity.getHocPhi(),enity.getThoiLuong(),
             enity.getMoTa(),enity.getHinh(),enity.getMaCD());
    }

    @Override
    public void delete(String macd) {
      String sql ="DELETE from ChuyenDe WHERE MaCD=?";
      JdbcHelper.update(sql, macd);
    }

    @Override
    public List<ChuyenDe> selectAll() {
        String sql ="select*from ChuyenDe";
        return selectBySql(sql);
    }

    @Override
    public ChuyenDe selectById(String macd) {
              String sql ="select*from ChuyenDe where MaCD=?";
        List<ChuyenDe>list =selectBySql(sql,macd);
        if(list.isEmpty()){
            return null;
        }
        return list.get(0);
    }

    @Override
    protected List<ChuyenDe> selectBySql(String sql, Object... args) {
         List<ChuyenDe> list =new ArrayList<ChuyenDe>();
           try{
            ResultSet rs = JdbcHelper.query(sql, args);
               while (rs.next()) { 
        ChuyenDe entity = new ChuyenDe();
        entity .setMaCD(rs.getString("MaCD"));
        entity .setTenCD(rs.getString("TenCD"));
        entity .setHocPhi(rs.getFloat("HocPhi"));
        entity .setThoiLuong(rs.getInt("ThoiLuong"));
        entity.setHinh(rs.getString("Hinh"));
        list.add(entity);     
               }
               rs.getStatement().getConnection().close();
         
               return list;
        }
        catch (SQLException ex) {
            throw new RuntimeException(ex);
                    
        }
    }
}
