/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import Enity.Nhanvien;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class NhanVienDAO extends EduSysDAO<Nhanvien,String>{

    @Override
    public void insert(Nhanvien enity) {
     String sql="INSERT INTO NhanVien(MaNV,MatKhau,HoTen,VaiTro) VALUEs (?,?,?,?)";
     JdbcHelper.update(sql,
             enity.getMaNV(),enity.getMatkhau(),
             enity.getHoten(),enity.isVaiTro());
    }

    @Override
    public void update(Nhanvien enity) {
        String sql ="Update NhanVien set MatKhau=?,HoTen=?,VaiTro=? where MaNV=?";
        JdbcHelper.update(sql,
        enity.getMatkhau(),enity.getHoten(),
        enity.isVaiTro(),enity.getMaNV());
    }

    @Override
    public void delete(String MaNV) {
      String sql ="DELETE from NhanVien WHERE MaNV=?";
      JdbcHelper.update(sql, MaNV);
    }

    @Override
    public List<Nhanvien> selectAll() {
        String sql ="select*from NhanVien";
        return selectBySql(sql);
    }

    @Override
    public Nhanvien selectById(String manv) {
        String sql ="select*from NhanVien where MaNV=?";
        List<Nhanvien>list =this.selectBySql(sql,manv);
        if(list.isEmpty()){
            return null;
        }
        return list.get(0);
       
    }

    @Override
    protected List<Nhanvien> selectBySql(String sql, Object... args) {
        
            List<Nhanvien> list =new ArrayList<Nhanvien>();
           try{
            ResultSet rs = JdbcHelper.query(sql, args);
               while (rs.next()) { 
        Nhanvien entity = new Nhanvien();
        entity .setMaNV(rs.getString("MaNV"));
        entity .setMatkhau(rs.getString("MatKhau"));
        entity .setHoten(rs.getString("HoTen"));
        entity .setVaiTro(rs.getBoolean("VaiTro"));
         list.add(entity);     
               }
               rs.getStatement().getConnection().close();
         
               return list;
        }
        catch (SQLException ex) {
            throw new RuntimeException(ex);
                    
        }
      
    }
            


   
  

   
    
}
