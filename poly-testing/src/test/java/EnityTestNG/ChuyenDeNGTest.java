/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnityTestNG;

import Enity.ChuyenDe;
import org.testng.Assert;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
public class ChuyenDeNGTest {
    ChuyenDe instance = new ChuyenDe();
    public ChuyenDeNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }


    @Test
    public void testGetMaCD() {
                String expResult = null;
        String result = instance.getMaCD();
        Assert.assertEquals(result, expResult);
    }

    @Test
    public void testSetMaCD() {
        String expected = "CD001";
        instance.setMaCD(expected);
        Assert.assertEquals(instance.getMaCD(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaCDNull() {
        String expected = null;
        instance.setMaCD(null);
        Assert.assertEquals(instance.getMaCD(), expected);
    }

    @Test
    public void testGetTenCD() {
        String expResult = null;
        instance.setMaCD(null);
        Assert.assertEquals(instance.getTenCD(), expResult);
    }

    @Test
    public void testSetTenCD() {
        String expected = "Cosodulieu";
        instance.setTenCD("Cosodulieu");
        Assert.assertEquals(instance.getTenCD(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetTenCDNull() {
        String expected = null;
        instance.setTenCD(null);
        Assert.assertEquals(instance.getTenCD(), expected);
    }

    @Test
    public void testGetHocPhi() {
        float expResult = 0.0F;
        float result = instance.getHocPhi();
        Assert.assertEquals(result, expResult, 0.0);
    }

    @Test
    public void testSetHocPhi() {
        float expected = 10;
        instance.setHocPhi(10);
        Assert.assertEquals(instance.getHocPhi(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetHocPhiNegative() {
        float expected = -10;
        instance.setHocPhi(-10);
        Assert.assertEquals(instance.getHocPhi(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetHocPhiZero() {
        float expected = 0;
        instance.setHocPhi(0);
        Assert.assertEquals(instance.getHocPhi(), expected);
    }

    @Test
    public void testGetThoiLuong() {
        int expResult = 0;
        int result = instance.getThoiLuong();
        Assert.assertEquals(result, expResult);
    }

    @Test
    public void testSetThoiLuong() {
        int expected = 3;
        instance.setThoiLuong(3);
        assertEquals(instance.getThoiLuong(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetThoiLuongZero() {
        int expected = 0;
        instance.setThoiLuong(0);
        assertEquals(instance.getThoiLuong(), expected);
    }

    @Test
    public void testGetHinh() {
        String expected = null;
        String result = instance.getHinh();
        assertEquals(result, expected);
    }

    @Test
    public void testSetHinh() {
        String expected = "D:\\ảnh\\ảnh đi chơi\\huế\\1.jpg";
        instance.setHinh("D:\\ảnh\\ảnh đi chơi\\huế\\1.jpg");
        assertEquals(instance.getHinh(), expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetHinhNull() {
        String expected = "";
        instance.setHinh("");
        assertEquals(instance.getHinh(), expected);
    }

    @Test
    public void testGetMoTa() {
        String expected = null;
        String result = instance.getMoTa();
        assertEquals(result, expected);
    }

    @Test
    public void testSetMoTa() {
        String expected = null;
        instance.setMoTa(null);
        assertEquals(instance.getMoTa(), expected);
    }
    
}
