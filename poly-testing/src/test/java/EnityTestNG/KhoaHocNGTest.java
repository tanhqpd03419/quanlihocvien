/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnityTestNG;

import Enity.KhoaHoc;
import java.util.Date;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
public class KhoaHocNGTest {
    KhoaHoc instance = new KhoaHoc();
    public KhoaHocNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }


    @Test
    public void testGetMaKH() {
        int expResult = 0;
        int result = instance.getMaKH();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaKH() {
        int expResult = 1;
        instance.setMaKH(1);
        assertTrue(instance.getMaKH() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaKHNull() {
        int expResult = 0;
        instance.setMaKH(0);
        assertTrue(instance.getMaKH() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaKHNegative() {
        int expResult = -1;
        instance.setMaKH(-1);
        assertTrue(instance.getMaKH() == expResult);
    }

    @Test
    public void testGetMaCD() {
        String expResult = null;
        String result = instance.getMaCD();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaCD() {
        String expResult = "1";
        instance.setMaCD("1");
        assertEquals(instance.getMaCD(), expResult);
    }

    @Test
    public void testGetHocPhi() {
        float expResult = 0;
        float result = instance.getHocPhi();
        assertTrue(result == expResult);
    }

    @Test
    public void testSetHocPhi() {
        float expResult = 20;
        instance.setHocPhi(20);
        assertTrue(instance.getHocPhi() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetHocPhiNull() {
        float expResult = 0;
        instance.setHocPhi(0);
        assertTrue(instance.getHocPhi() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetHocPhiNegative() {
        float expResult = -10;
        instance.setHocPhi(-10);
        assertTrue(instance.getHocPhi() == expResult);
    }

    @Test
    public void testGetThoiLuong() {
        int expResult = 0;
        int result = instance.getThoiLuong();
        assertTrue(result == expResult);
    }

    @Test
    public void testSetThoiLuong() {
        int expResult = 20;
        instance.setThoiLuong(20);
        assertTrue(instance.getThoiLuong() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetThoiLuongNull() {
        int expResult = 0;
        instance.setThoiLuong(0);
        assertTrue(instance.getThoiLuong() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetThoiLuongNegative() {
        int expResult = -10;
        instance.setThoiLuong(-10);
        assertTrue(instance.getThoiLuong() == expResult);
    }

    @Test
    public void testGetNgayKG() {
        Date expResult = null;
        Date result = instance.getNgayKG();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetNgayKG() {
        Date expected = new Date();
	instance.setNgayKG(new Date());
	Date actual = instance.getNgayKG();
	assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetNgayKGNull() {
        Date expected = null;
	instance.setNgayKG(null);
	Date actual = instance.getNgayKG();
	assertEquals(actual, expected);
    }

    @Test
    public void testGetGhiChu() {
        String expResult = null;
        String result = instance.getGhiChu();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetGhiChu() {
        String expResult = "text";
        instance.setGhiChu("text");
        assertEquals(instance.getGhiChu(), expResult);
    }

    @Test
    public void testGetMaNV() {
        String expResult = null;
        String result = instance.getMaNV();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaNV() {
        String expResult = "123";
        instance.setMaNV("123");
        assertEquals(instance.getMaNV(), expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaNVNull() {
        String expResult = "";
        instance.setMaNV("");
        assertEquals(instance.getMaNV(), expResult);
    }

    @Test
    public void testGetNgayTao() {
        Date expected = new Date();
	instance.setNgayKG(new Date());
	Date actual = instance.getNgayKG();
	assertEquals(actual, expected);
    }

    @Test
    public void testSetNgayTao() {
        Date expected = new Date();
		instance.setNgayTao(new Date());
		Date actual = instance.getNgayTao();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetNgayTaoNull() {
        Date expected = null;
	instance.setNgayKG(null);
	Date actual = instance.getNgayKG();
	assertEquals(actual, expected);
    }

    @Test
    public void testGetNguoiTao() {
        String expResult = null;
        String result = instance.getNguoiTao();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetNguoiTao() {
        String expResult = "hoc";
        instance.setNguoiTao("hoc");
        assertEquals(instance.getNguoiTao(), expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetNguoiTaoNull() {
        String expResult = "";
        instance.setNguoiTao("");
        assertEquals(instance.getNguoiTao(), expResult);
    }
    
}
