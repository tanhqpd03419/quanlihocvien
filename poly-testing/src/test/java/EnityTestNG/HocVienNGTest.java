/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnityTestNG;

import Enity.HocVien;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
public class HocVienNGTest {
    HocVien instance = new HocVien();
    public HocVienNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }


    @Test
    public void testGetMaHV() {
        int expResult = 0;
        int result = instance.getMaHV();
        assertTrue(instance.getMaHV()== expResult);
    }

    @Test
    public void testSetMaHV() {
        int expResult = 1;
        instance.setMaHV(1);
        assertTrue(instance.getMaHV()== expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaHVNegative() {
        int expResult = -1;
        instance.setMaHV(-1);
        assertTrue(instance.getMaHV()== expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaHVZero() {
        int expResult = 0;
        instance.setMaHV(0);
        assertTrue(instance.getMaHV()== expResult);
    }
    

    @Test
    public void testGetMaKH() {
        int expResult = 0;
        int result = instance.getMaKH();
        assertTrue(instance.getMaKH()== expResult);
    }

    @Test
    public void testSetMaKH() {
        int expResult = 1;
        instance.setMaKH(1);
        assertTrue(instance.getMaKH()== expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaKHNull() {
        int expResult = 0;
        instance.setMaKH(0);
        assertTrue(instance.getMaKH()== expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaKHNegative() {
        int expResult = -1;
        instance.setMaKH(-1);
        assertTrue(instance.getMaKH()== expResult);
    }

    @Test
    public void testGetMaNH() {
        String expResult = null;
        String result = instance.getMaNH();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaNH() {
        String expResult = "123";
        instance.setMaNH("123");
        assertEquals(instance.getMaNH(), expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetMaNHNull() {
        String expResult = null;
        instance.setMaNH(null);
        assertEquals(instance.getMaNH(), expResult);
    }

    @Test
    public void testGetDiem() {
        float expResult = 0.0F;
        float result = instance.getDiem();
        assertEquals(result, expResult, 0.0);
    }

    @Test
    public void testSetDiem() {
        float expResult = 2.0F;
        instance.setDiem(2.0F);
        assertTrue(instance.getDiem() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetDiemNull() {
        float expResult = 0;
        instance.setDiem(0);
        assertTrue(instance.getDiem() == expResult);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetDiemNegative() {
        float expResult = -1;
        instance.setDiem(-1);
        assertTrue(instance.getDiem() == expResult);
    }
    
}
