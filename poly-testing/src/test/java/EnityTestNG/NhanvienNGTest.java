/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnityTestNG;

import Enity.Nhanvien;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
public class NhanvienNGTest {
    Nhanvien instance = new Nhanvien();
    public NhanvienNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test
    public void testGetMaNV() {
        String expResult = null;
        String result = instance.getMaNV();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaNV() {
        String expected = "NV010";
		instance.setMaNV("NV010");
		String actual = instance.getMaNV();
		assertEquals(actual, expected);	
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setMaNVNullTest() {
		String expected = null;
		instance.setMaNV(null);
		String actual = instance.getMaNV();
		assertEquals(actual, expected);	
	}

    @Test
    public void testGetMatkhau() {
        String expResult = null;
        String result = instance.getMatkhau();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMatkhau() {
        String expected = "123456";
		instance.setMatkhau("123456");
		String actual = instance.getMatkhau();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setMatKhauNullTest() {
		String expected = null;
		instance.setMatkhau(null);
		String actual = instance.getMatkhau();
		assertEquals(actual, expected);
	}

    @Test
    public void testIsVaiTro() {
        boolean expResult = false;
        boolean result = instance.isVaiTro();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetVaiTro() {
        boolean expected = true;
		instance.setVaiTro(true);
		boolean actual =  instance.isVaiTro();
		assertEquals(actual, expected);
    }
    
    @Test
	public void setVaiTrofalseTest() {
		boolean expected = false;
		instance.setVaiTro(false);
		boolean actual =  instance.isVaiTro();
		assertEquals(actual, expected);
	}

    @Test
    public void testGetHoten() {
        String expResult = null;
        String result = instance.getHoten();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetHoten() {
        String expected = "Nguyen Van Teo";
		instance.setHoten("Nguyen Van Teo");
		String actual = instance.getHoten();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setHoTenNullTest() {
		String expected = null;
		instance.setHoten(null);
		String actual = instance.getHoten();
		assertEquals(actual, expected);
	}
    
}
