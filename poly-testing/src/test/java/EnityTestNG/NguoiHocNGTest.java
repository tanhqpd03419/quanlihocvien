/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnityTestNG;

import Enity.NguoiHoc;
import com.eduSys.utils.XDate;
import java.util.Date;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
public class NguoiHocNGTest {
    NguoiHoc instance = new NguoiHoc();
    public NguoiHocNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test
    public void testGetMaNH() {
        String expResult = null;
        String result = instance.getMaNH();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaNH() {
        System.out.println("setMaNH");
        String MaNH = "";
        NguoiHoc instance = new NguoiHoc();
        instance.setMaNH(MaNH);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetHoTen() {
        String expResult = null;
        String result = instance.getHoTen();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetHoTen() {
        System.out.println("setHoTen");
        String HoTen = "";
        NguoiHoc instance = new NguoiHoc();
        instance.setHoTen(HoTen);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetNgaySinh() {
        Date expected = null;
	Date actual = instance.getNgaySinh();
	assertEquals(actual, expected);
    }

    @Test
    public void testSetNgaySinh() {
        Date expected = XDate.toDate("22/12/2000", "dd/MM/yyyy");
		instance.setNgayDK(XDate.toDate("22/12/2000", "dd/MM/yyyy"));
		Date actual = instance.getNgayDK();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setNgaySinhNullTest() {
	Date expected = null;
	instance.setNgayDK(null);
	Date actual = instance.getNgayDK();
	assertEquals(actual, expected);
	}

    @Test
    public void testIsGioiTinh() {
        boolean expResult = false;
        boolean result = instance.isGioiTinh();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetGioiTinhTrue() {
        boolean expected = true;
		instance.setGioiTinh(true);
		boolean actual = instance.isGioiTinh();
		assertEquals(actual, expected);
    }
    
    @Test
    public void testSetGioiTinhFalse() {
        boolean expected = false;
		instance.setGioiTinh(false);
		boolean actual = instance.isGioiTinh();
		assertEquals(actual, expected);
    }

    @Test
    public void testGetDienThoai() {
        int expResult = 0;
        int result = instance.getDienThoai();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetDienThoai() {
        int expected = 384793266;
	instance.setDienThoai(384793266);
	int actual = instance.getDienThoai();
	assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void testSetDienThoaiNull() {
        int expected = 0;
	instance.setDienThoai(0);
	int actual = instance.getDienThoai();
	assertEquals(actual, expected);
    }

    @Test
    public void testGetEmail() {
        String expResult = null;
        String result = instance.getEmail();
        assertEquals(result, expResult);
    }

    @Test
	public void setEmailTest() {
		String expected = "duy@gmail.com";
		instance.setEmail("duy@gmail.com");
		String actual = instance.getEmail();
		assertEquals(actual, expected);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void setEmailNullTest() {
		String expected = null;
		instance.setEmail(null);
		String actual = instance.getEmail();
		assertEquals(actual, expected);
	}

    @Test
    public void testGetGhiChu() {
        String expResult = null;
        String result = instance.getGhiChu();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetGhiChu() {
        String expected = "testNG";
		instance.setGhiChu("testNG");;
		String actual = instance.getGhiChu();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setGhiChuEmptyTest() {
		String expected = "";
		instance.setGhiChu("");;
		String actual = instance.getGhiChu();
		assertEquals(actual, expected);
	}

    @Test
    public void testGetMaNV() {
        String expResult = null;
        String result = instance.getMaNV();
        assertEquals(result, expResult);
    }

    @Test
    public void testSetMaNV() {
        String expected = "NV001";
	instance.setMaNV("NV001");
	String actual = instance.getMaNV();
	assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setMaNVNullTest() {
		String expected = null;
		instance.setMaNV(null);
		String actual = instance.getMaNV();
		assertEquals(actual, expected);
	}

    @Test
    public void testGetNgayDK() {
        String expected = XDate.toString(XDate.now());
		String actual = XDate.toString(instance.getNgayDK());
		assertEquals(actual, expected);
    }

    @Test
    public void testSetNgayDK() {
        Date expected = XDate.toDate("29/06/2020", "dd/MM/yyyy");
		instance.setNgayDK(XDate.toDate("29/06/2020", "dd/MM/yyyy"));
		Date actual = instance.getNgayDK();
		assertEquals(actual, expected);
    }
    
    @Test(expectedExceptions = Exception.class)
	public void setNgayDKNullTest() {
		Date expected = null;
		instance.setNgayDK(null);
		Date actual = instance.getNgayDK();
		assertEquals(actual, expected);
	}
    
}
