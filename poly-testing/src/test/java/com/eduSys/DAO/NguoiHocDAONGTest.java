/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import Enity.NguoiHoc;
import com.eduSys.utils.JdbcHelper;
import com.eduSys.utils.XDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.testng.Assert;
import static org.testng.Assert.*;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
@PrepareForTest({JdbcHelper.class, NguoiHocDAO.class })
public class NguoiHocDAONGTest {
    
  NguoiHocDAO dao;
	NguoiHocDAO daoSpy;
	
	@ObjectFactory
	public IObjectFactory getObjectFactory() {
		return new org.powermock.modules.testng.PowerMockObjectFactory();
	}

	@BeforeMethod
	public void beforeMethod() {
		dao = new NguoiHocDAO();
		PowerMockito.mockStatic(JdbcHelper.class);
		daoSpy = PowerMockito.spy(new NguoiHocDAO());
	}
	
	@AfterMethod
	public void afterMethod() {
		dao = null;
		daoSpy = null;
	}

	@Test
	public void deleteWithMaNhNullTest() {
		String maCd = null;
		dao.delete(maCd);
	}

	@Test
	public void deleteWithMaNhValidTest() {
		String maCd = "NH010";
		dao.delete(maCd);
	}

	@Test(expectedExceptions = Exception.class)
	public void insertWithNullModelTest() {
		          NguoiHoc model = null;
		dao.insert(model);
	}

	@Test
	public void insertWithEmptyModelTest() {
		NguoiHoc model = new NguoiHoc();
		dao.insert(model);
	}

	@Test
	public void insertWithValidModelTest() {
		NguoiHoc model = new NguoiHoc();
		model.setDienThoai(012345);
		model.setEmail("mam@gmail.com");
		model.setGhiChu("testng");
		model.setGioiTinh(false);
		model.setHoTen("Nguyễn thị Mắm");
		model.setMaNH("NH010");
		model.setMaNV("NV001");
		model.setNgayDK(new Date());
		model.setNgaySinh( XDate.toDate("22/12/2000", "dd/MM/yyyy"));
		dao.insert(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void selectTest() throws Exception{
			List<NguoiHoc> expecteds = new ArrayList<>();
			PowerMockito.doReturn(expecteds).when(daoSpy, "selectAll", ArgumentMatchers.anyString(),
					ArgumentMatchers.any());
			List<NguoiHoc> actuals = daoSpy.selectAll();

			Assert.assertEquals(expecteds, actuals);
	}

	@Test(expectedExceptions = Exception.class)
	public void updateWithNullModelTest() {
		NguoiHoc model = null;
		dao.update(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void updateWithEmptyModelTest() {
		NguoiHoc model = new NguoiHoc();
		dao.update(model);
	}

	@Test
	public void updateWithValidModelTest() {
		NguoiHoc model = new NguoiHoc();
		model.setDienThoai(0123);
		model.setEmail("mam@gmail.com");
		model.setGhiChu("testnggggg");
		model.setGioiTinh(false);
		model.setHoTen("Nguyễn thị Mắm");
		model.setMaNH("NH010");
		model.setMaNV("NV001");
		model.setNgayDK(new Date());
		model.setNgaySinh( XDate.toDate("22/12/2000", "dd/MM/yyyy"));
		dao.update(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void findByIdWithValidModelTest() throws Exception{
			String maNh = "NH010";
			
			NguoiHoc expected = new NguoiHoc();
			List<NguoiHoc> resultList = new ArrayList<>();
			resultList.add(expected);

			PowerMockito.doReturn(resultList).when(daoSpy, "selectById",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			NguoiHoc result = daoSpy.selectById(maNh);
			
			Assert.assertEquals(result, expected);		
	}
	
	@Test(expectedExceptions = Exception.class)
	public void findByIdWithNotFoundTest() throws Exception{
			String maNH = "NH111";

			NguoiHoc expected = null;
			List<NguoiHoc> resultList = new ArrayList<>();

			PowerMockito.doReturn(resultList).when(daoSpy, "select",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			NguoiHoc result = daoSpy.selectById(maNH);

			Assert.assertEquals(result, expected);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void selectByCourseTest() throws Exception{
		Integer maKH = 111;

		NguoiHoc nh = new NguoiHoc();
		List<NguoiHoc> expected = new ArrayList<>();
		expected.add(nh);
		
		PowerMockito.doReturn(expected).when(daoSpy, "selectByCourse",
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any());

		List<NguoiHoc> result = daoSpy.selectNotlnCourse(111, "abc");

		Assert.assertEquals(result, expected);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void selectByCourseNotFoundTest() throws Exception{
		Integer maKH = 111;

		List<NguoiHoc> expected = new ArrayList<>();
		
		PowerMockito.doReturn(expected).when(daoSpy, "select",
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any());

		List<NguoiHoc> result = daoSpy.SelectByKeyword(maKH.toString());

		Assert.assertEquals(result, expected);
	}

	@Test(expectedExceptions = Exception.class)
	public void selectByKeywordTest() throws Exception{
		String name = "dat";

		NguoiHoc nh = new NguoiHoc();
		List<NguoiHoc> expected = new ArrayList<>();
		expected.add(nh);

		PowerMockito.doReturn(expected).when(daoSpy, "select",
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any());

		List<NguoiHoc> result = daoSpy.SelectByKeyword(name);

		Assert.assertEquals(result, expected);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void selectByKeywordNotFoundTest() throws Exception{
		String name = "abc";
		
		List<NguoiHoc> expected = new ArrayList<>();	
		PowerMockito.doReturn(expected).when(daoSpy, "select",
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any());

		List<NguoiHoc> result = daoSpy.SelectByKeyword(name);

		Assert.assertEquals(result, expected);
	}
}
