/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import Enity.HocVien;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import static org.testng.Assert.*;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
@PrepareForTest({ HocVienDAO.class, JdbcHelper.class })
public class HocVienDAONGTest {
    
    HocVienDAO hocVienDAO;
    HocVienDAO HocVienDAOSpy;
    public HocVienDAONGTest() {
    }
  @ObjectFactory
    public IObjectFactory getIObjectFactory() {
        return new org.powermock.modules.testng.PowerMockObjectFactory();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        hocVienDAO= new HocVienDAO();
        PowerMockito.mockStatic(JdbcHelper.class);
        HocVienDAOSpy = PowerMockito.spy(new HocVienDAO());
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test
    public void testInsertWithNoEnity() {
        //System.out.println("insert");
        HocVien enity = new HocVien();
         
        hocVienDAO.insert(enity);

    }
    @Test(expectedExceptions = Exception.class)
	public void insertWithNullModelTest() {
		HocVien enity = null;
              
		hocVienDAO.insert(enity);
	}
      @Test
	public void insertWithValidModelTest() {
		HocVien model = new HocVien();
		model.setDiem(10);
		model.setMaHV(16);
		model.setMaKH(1);
		model.setMaNH("NH001");
		hocVienDAO.insert(model);
	}

    @Test(expectedExceptions = Exception.class)
    public void testUpdateWithnullEnity() {
        
        HocVien enity = null;
        
        hocVienDAO.update(enity);

    }

    @Test
    public void testUpdateWithEnity() {
        //System.out.println("update");
        HocVien enity = new HocVien();
       
       hocVienDAO.update(enity);

    }

    @Test
    public void testUpdateWithvalidEnity() {
        //System.out.println("update");
        HocVien enity = new HocVien();
       HocVien model = new HocVien();
		model.setDiem(10);
		model.setMaHV(16);
		model.setMaKH(1);
		model.setMaNH("NH001");
		hocVienDAO.insert(model);

    }

   @Test
	public void deleteWithNullModelTest() {
		String mahv= null;
		hocVienDAO.delete(mahv);
	}

	@Test
	public void deleteWithMaKhValidTest() {
		String mahv = "12";
		hocVienDAO.delete(mahv);
	}

	

    @Test
    public void testSelectAll() {
        
    }

    @Test
    public void testSelectById_Integer() {
    }

    @Test
    public void testSelectBySql() {
    }

    @Test
    public void testSelectByKhoaHoc() {
    }

    @Test
    public void testSelectById_String() {
    }
    
}
