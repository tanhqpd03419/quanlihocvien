/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import com.eduSys.utils.JdbcHelper;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import com.mockrunner.mock.jdbc.MockStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.Assert;
import static org.testng.Assert.assertEquals;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */

@PrepareForTest({JdbcHelper.class ,ThongKeDAO.class})
public class ThongKeDAOTestNG extends PowerMockTestCase {

	ThongKeDAO daoSpy;

	@Mock
	MockConnection connect;
	@Mock
	MockStatement statement;
	@Spy
	@InjectMocks
	MockResultSet rs = new MockResultSet("myMock");

	@ObjectFactory
	public IObjectFactory getObjectFactory() {
		return new org.powermock.modules.testng.PowerMockObjectFactory();
	}

	@BeforeMethod
	public void beforeMethod() {
		PowerMockito.mockStatic(JdbcHelper.class);
		daoSpy = PowerMockito.spy(new ThongKeDAO());

	}

	@AfterMethod
	public void afterMethod() {
		rs = new MockResultSet("myMock");
	}

	@Test
	public void getBangDiemTest() throws Exception{
		rs = initBangDiemMockResultTest();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString(),
				ArgumentMatchers.anyString()))
				.thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getBangDiem(10).size();
		System.out.println("-----" + result);
		Assert.assertEquals(result, 1);
	}
	
	@Test
	public void getBangDiemMultipleDataTest() throws Exception{
		rs = initBangDiemMultipleDataMockResultSet();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString(),
				ArgumentMatchers.anyString()))
				.thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		List<Integer> listResult = new ArrayList<>();
		
		int result = daoSpy.getBangDiem(19).size();
		int result1 = daoSpy.getBangDiem(24).size();
		int result2 = daoSpy.getBangDiem(25).size();

		listResult.add(result);
		listResult.add(result2);
		listResult.add(result1);
		System.out.println("-----" + listResult.size());
		Assert.assertEquals(listResult.size(), 3);
	}

	@Test
	public void getDiemTheoChuyenDeTest() throws Exception {
		rs = initDiemMockResultTest();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getDiemChuyenDe().size();
		System.out.println("-----" + result);
		Assert.assertEquals(result, 1);
	}

	@Test
	public void getDiemTheoChuyenDeWithMultipleDataTest() throws Exception {
		rs = initDiemMultipleDataMockResultSet();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getDiemChuyenDe().size();
		System.out.println("-----" + result);
		Assert.assertEquals(result, 3);
	}

	@Test
	public void getDoanhThuTest() throws Exception {
		rs = initDtMockResultTest();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt()))
				.thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getDoanhThu(2019).size();
		System.out.println("-----" + result);
		Assert.assertEquals(result, 1);
	}

	@Test
	public void getDoanhThuWithMultipleDataTest() throws Exception {
		rs = initDtMultipleDataMockResultSet();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt()))
				.thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		List<Integer> listResult = new ArrayList<>();

		int result = daoSpy.getDoanhThu(2019).size();
		int result1 = daoSpy.getDoanhThu(2018).size();
		int result2 = daoSpy.getDoanhThu(2020).size();

		listResult.add(result);
		listResult.add(result2);
		listResult.add(result1);
		System.out.println("-----" + listResult.size());
		Assert.assertEquals(listResult.size(), 3);
	}

	@Test
	public void getNguoiHocTest() throws Exception {

		rs = initNhMockResultTest();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getLuongNguoiHoc().size();
		System.out.println("-----" + result);
		Assert.assertEquals(result, 1);
	}

	@Test
	public void getNguoiHocWithMultipleDataTest() throws Exception {
		rs = initNhMultipleDataMockResultSet();

		PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);

		PowerMockito.when(rs.getStatement()).thenReturn(statement);

		PowerMockito.when(statement.getConnection()).thenReturn(connect);

		int result = daoSpy.getLuongNguoiHoc().size();

		System.out.println("-----" + result);

		Assert.assertEquals(result, 3);
	}

	private MockResultSet initNhMockResultTest() throws Exception {
		rs.addColumn("Nam", new Integer[] { 2018 });
		rs.addColumn("SoLuong", new Integer[] { 10 });
		rs.addColumn("DauTien", new java.sql.Date[] { new java.sql.Date(new java.util.Date().getTime()) });
		rs.addColumn("CuoiCung", new java.sql.Date[] { new java.sql.Date(new java.util.Date().getTime()) });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initNhMultipleDataMockResultSet() throws Exception {
		rs.addColumn("Nam", new Integer[] { 2018, 2019, 2020 });
		rs.addColumn("SoLuong", new Integer[] { 10, 10, 17 });
		rs.addColumn("DauTien", new java.sql.Date[] { new java.sql.Date(new java.util.Date().getTime()),
				new java.sql.Date(new java.util.Date().getTime()), new java.sql.Date(new java.util.Date().getTime()) });
		rs.addColumn("CuoiCung", new java.sql.Date[] { new java.sql.Date(new java.util.Date().getTime()),
				new java.sql.Date(new java.util.Date().getTime()), new java.sql.Date(new java.util.Date().getTime()) });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initDiemMockResultTest() throws Exception {
		rs.addColumn("ChuyenDe", new String[] { "Java 1" });
		rs.addColumn("SoHV", new Integer[] { 10 });
		rs.addColumn("ThapNhat", new Double[] { 5.0 });
		rs.addColumn("CaoNhat", new Double[] { 10.0 });
		rs.addColumn("TrungBinh", new Double[] { 8.0 });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initDiemMultipleDataMockResultSet() throws Exception {
		rs.addColumn("ChuyenDe", new String[] { "Java 1", "SQL", "Angular" });
		rs.addColumn("SoHV", new Integer[] { 10, 13, 15 });
		rs.addColumn("ThapNhat", new Double[] { 5.0, 6.0, 7.0 });
		rs.addColumn("CaoNhat", new Double[] { 10.0, 8.0, 9.0 });
		rs.addColumn("TrungBinh", new Double[] { 7.5, 7.0, 8.0 });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initDtMockResultTest() throws Exception {
		rs.addColumn("ChuyenDe", new String[] { "Java 1" });
		rs.addColumn("SoKH", new Integer[] { 1 });
		rs.addColumn("SoHV", new Integer[] { 10 });
		rs.addColumn("DoanhThu", new Double[] { 900000.0 });
		rs.addColumn("ThapNhat", new Double[] { 300000.0 });
		rs.addColumn("CaoNhat", new Double[] { 300000.0 });
		rs.addColumn("TrungBinh", new Double[] { 300000.0 });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initDtMultipleDataMockResultSet() throws Exception {
		rs.addColumn("ChuyenDe", new String[] { "Java 1", "SQL", "Angular" });
		rs.addColumn("SoKH", new Integer[] { 1, 3, 5 });
		rs.addColumn("SoHV", new Integer[] { 3, 10, 10 });
		rs.addColumn("DoanhThu", new Double[] { 900000.0, 2000000.0, 3000000.0 });
		rs.addColumn("ThapNhat", new Double[] { 300000.0, 300000.0, 300000.0 });
		rs.addColumn("CaoNhat", new Double[] { 300000.0, 500000.0, 500000.0 });
		rs.addColumn("TrungBinh", new Double[] { 300000.0, 200000.0, 300000.0 });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initBangDiemMockResultTest() throws Exception {
		rs.addColumn("maNguoiHoc", new String[] { "NH001" });
		rs.addColumn("hoVaTen", new String[] { "Nguyen Thi Mam" });
		rs.addColumn("diemTrungBinh", new Double[] { 10.0 });
		rs.addColumn("xepLoai", new String[] { "Xuat Sac" });
		rs.beforeFirst();
		return rs;
	}

	private MockResultSet initBangDiemMultipleDataMockResultSet() throws Exception {
		rs.addColumn("maNguoiHoc", new String[] { "NH001", "NH002", "NH003" });
		rs.addColumn("hoVaTen", new String[] { "Nguyen Thi Mam", "Nguyen Thi Muoi", "Nguyen Van Teo" });
		rs.addColumn("diemTrungBinh", new Double[] { 10.0, 5.0, 7.0 });
		rs.addColumn("xepLoai", new String[] { "Xuat Sac", "TrungBinh", "Kha" });
		rs.beforeFirst();
		return rs;
	}
}

//public class ThongKeDAOTestNG extends PowerMockTestCase{
//    ThongKeDAO thongKeDAOSpy;
//   
//    MockConnection connection;
//    
//    MockStatement statement;
//    
//    MockResultSet rs = new MockResultSet("myMock");
//    
//    public ThongKeDAOTestNG() {
//    }
//    
//    @BeforeMethod
//    public void setUp() {
//        PowerMockito.mockStatic(JdbcHelper.class);
//        thongKeDAOSpy =PowerMockito.spy(new ThongKeDAO());
//        connection =PowerMockito.mock(MockConnection.class);
//        statement =PowerMockito.mock(MockStatement.class);
//        rs = PowerMockito.mock(MockResultSet.class);
//    }
//    
//    @AfterMethod
//    public void tearDown() {
//    }
//
//    @Test
//    public void testGetBangDiem()throws Exception {
//      System.out.println("GetBangDiem");
//        rs =initMockResultSetBDiem();
//        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
//        PowerMockito.when(rs.getStatement()).thenReturn(statement);
//        PowerMockito.when(statement.getConnection()).thenReturn(connection);
//        int result = thongKeDAOSpy.getBangDiem(18).size();
//        assertEquals(1,result) ;  
////       
////		List<Integer> listResult = new ArrayList<>();
////		
////		int result = thongKeDAOSpy.getBangDiem(19).size();
////		int result1 = thongKeDAOSpy.getBangDiem(24).size();
////		int result2 = thongKeDAOSpy.getBangDiem(25).size();
////
////		listResult.add(result);
////		listResult.add(result2);
////		listResult.add(result1);
////		System.out.println("-----" + listResult.size());
////		Assert.assertEquals(listResult.size(), 3);
//    }
//
//    @Test
//    public void testGetLuongNguoiHoc()throws Exception{
//        System.out.println("GetLuongNguoiHoc");
//        rs =initMockResultSet();
//        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
//        PowerMockito.when(rs.getStatement()).thenReturn(statement);
//        PowerMockito.when(statement.getConnection()).thenReturn(connection);
//        List result = thongKeDAOSpy.getLuongNguoiHoc();
//        assertEquals(0,result.size()) ;
//        
//    }
//       @Test
//    public void testGetLuongNguoiHocWithMultipleData()throws Exception{
//        System.out.println("GetLuongNguoiHoc");
//        rs =initMultipleDataMockResultSet();
//        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
//        PowerMockito.when(rs.getStatement()).thenReturn(statement);
//        PowerMockito.when(statement.getConnection()).thenReturn(connection);
//        List result = thongKeDAOSpy.getLuongNguoiHoc();
//        assertEquals(0, result.size());
//        
//    }
//
//    @Test
//    public void testGetDiemChuyenDe()throws Exception{
//        System.out.println("Getdiemchuyende");
//        rs =initMockResultSetChuyende();
//        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
//        PowerMockito.when(rs.getStatement()).thenReturn(statement);
//        PowerMockito.when(statement.getConnection()).thenReturn(connection);
//        List result = thongKeDAOSpy.getDiemChuyenDe();
//        assertEquals(0,result.size()) ;
//        
//    }
//
//    @Test
//    public void testGetDoanhThu() throws Exception{
//        System.out.println("GetDoanhthu");
//        rs =initMockResultSetDoanhThu();
//        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
//        PowerMockito.when(rs.getStatement()).thenReturn(statement);
//        PowerMockito.when(statement.getConnection()).thenReturn(connection);
//        List result = thongKeDAOSpy.getDoanhThu(2000);
//        assertEquals(0,result.size()) ;
//    }
//
//    private MockResultSet initMockResultSet() throws SQLException {
//        rs.addColumn("Nam",new Integer[]{1});
//        rs.addColumn("soluong", new Integer[]{1});
//        rs.addColumn("Dautien", new java.sql.Date[] {
//        new java.sql.Date(new java.util.Date().getTime())});
//        rs.addColumn("cuoicung", new java.sql.Date[] {
//        new java.sql.Date(new java.util.Date().getTime())});
//        rs.beforeFirst();
//    
//        
//        return rs;
//    }
//
//    private MockResultSet initMultipleDataMockResultSet() throws SQLException {
//        rs.addColumn("Nam",new Integer[]{1,2,5});
//        rs.addColumn("soluong", new Integer[]{1,5,7});
//        rs.addColumn("Dautien", new java.sql.Date[] {
//              new java.sql.Date(new java.util.Date().getTime()),
//              new java.sql.Date(new java.util.Date().getTime()),
//              new java.sql.Date(new java.util.Date().getTime())
//            
//        });
//        rs.addColumn("cuoicung", new java.sql.Date[] {
//        new java.sql.Date(new java.util.Date().getTime()),
//            new java.sql.Date(new java.util.Date().getTime()),
//            new java.sql.Date(new java.util.Date().getTime())
//        });
//        rs.beforeFirst();
//    
//        
//        return rs;
//    }
//
//    private MockResultSet initMockResultSetBDiem() throws SQLException {
//        rs.addColumn("MaNH",new String[] { "NH001" });
//        rs.addColumn("HoTen",new String[] { "Nguyen Thi Mam" });
//        rs.addColumn("Diem", new Double[] { 10.0 });
//        rs.beforeFirst();
//       return rs;
//    
//}
// 
//
//    private MockResultSet initMockResultSetChuyende() throws SQLException {
//        rs.addColumn("ChuyenDe",new String[]{"java"});
//        rs.addColumn("SoHV", new Integer[]{1});
//        rs.addColumn("ThapNhat", new Double[]{7.0});
//        rs.addColumn("CaoNhat", new Double[]{9.0});
//        rs.addColumn("TrungBinh", new Double[]{8.0});
//        rs.beforeFirst();
//       return rs;   
//    }
//       private MockResultSet initMultipleDataMockResultSetChuyenDe() throws Exception {
//		rs.addColumn("ChuyenDe", new String[] { "Java 1", "SQL", "Angular" });
//		rs.addColumn("SoHV", new Integer[] { 10, 13, 15 });
//		rs.addColumn("ThapNhat", new Double[] { 5.0, 6.0, 7.0 });
//		rs.addColumn("CaoNhat", new Double[] { 10.0, 8.0, 9.0 });
//		rs.addColumn("TrungBinh", new Double[] { 7.5, 7.0, 8.0 });
//		rs.beforeFirst();
//		return rs;
//	}
//
//    private MockResultSet initMockResultSetDoanhThu()throws SQLException {
//        rs.addColumn("ChuyenDe",new String[]{"java"});
//        rs.addColumn("SoKH", new Integer[]{1});
//        rs.addColumn("SoHV", new Integer[]{1});
//        rs.addColumn("DoanhThu", new Integer[]{1});
//        rs.addColumn("ThapNhat", new Integer[]{1});
//        rs.addColumn("CaoNhat", new Integer[]{1});
//        rs.addColumn("TrungBinh", new Integer[]{1});
//        rs.beforeFirst();
//       return rs; 
//    }
//}

