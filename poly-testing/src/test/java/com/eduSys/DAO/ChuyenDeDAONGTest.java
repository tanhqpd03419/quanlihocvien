/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import Enity.ChuyenDe;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.testng.Assert;
import static org.testng.Assert.*;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
@PrepareForTest({ JdbcHelper.class, ChuyenDeDAO.class})
public class ChuyenDeDAONGTest {
   ChuyenDeDAO dao;
	ChuyenDeDAO daoSpy;
	
	@ObjectFactory
	public IObjectFactory getObjectFactory() {
		return new org.powermock.modules.testng.PowerMockObjectFactory();
	}
	
	@BeforeMethod
	public void beforeMethod() {
		dao = new ChuyenDeDAO();
		PowerMockito.mockStatic(JdbcHelper.class);
		daoSpy = PowerMockito.spy(new ChuyenDeDAO());
	}

	@AfterMethod
	public void afterMethod() {
		dao = null;
		daoSpy = null;
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

	@Test
	public void deleteWithMaCdNullTest() {
		String maCd = null;
		dao.delete(maCd);
	}

	@Test
	public void deleteWithMaKhValidTest() {
		String maCd = "CD003";
		dao.delete(maCd);
	}

	@Test(expectedExceptions = Exception.class)
	public void insertWithNullModelTest() {
		ChuyenDe model = null;
		dao.insert(model);
	}

	@Test
	public void insertWithEmptyModelTest() {
		ChuyenDe model = new ChuyenDe();
		dao.insert(model);
	}

	@Test
	public void insertWithValidModelTest() {
		ChuyenDe model = new ChuyenDe();
		model.setHinh("C:\\Users\\ASDZ\\Pictures\\Family\\PD03134.jpg");
		model.setHocPhi(250000);
		model.setMaCD("CD001");
		model.setMoTa("testNG");
		model.setTenCD("SQL");
		model.setThoiLuong(3);
		dao.insert(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void selectTest() throws Exception{
			List<ChuyenDe> expecteds = new ArrayList<>();
			PowerMockito.doReturn(expecteds).when(daoSpy, "selectAll",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());
			List<ChuyenDe> actuals = daoSpy.selectAll();

			Assert.assertEquals(expecteds, actuals);
	}

	@Test(expectedExceptions = Exception.class)
	public void updateWithNullModelTest() {
		ChuyenDe model = null;
		dao.update(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void updateWithEmptyModelTest() {
		ChuyenDe model = new ChuyenDe();
		dao.update(model);
	}

	@Test
	public void updateWithValidModelTest() {
		ChuyenDe model = new ChuyenDe();
		model.setHinh("C:\\Users\\ASDZ\\Pictures\\Family\\PD03134.jpg");
		model.setHocPhi(300000);
		model.setMaCD("CD001");
		model.setMoTa("testNG");
		model.setTenCD("SQL1");
		model.setThoiLuong(5);
		dao.update(model);
	}

	@Test(expectedExceptions = Exception.class)
	public void findByIdWithValidModelTest() throws Exception{
			String maCd = "";
			
			ChuyenDe expected = new ChuyenDe();
			List<ChuyenDe> resultList = new ArrayList<>();
			resultList.add(expected);

			PowerMockito.doReturn(resultList).when(daoSpy, "selectById",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			ChuyenDe result = daoSpy.selectById(maCd);
			
			System.out.println("--- ^^^ ++++ " + result);
			System.out.println("--- ^^^ ++++ " + expected);
			
			Assert.assertEquals(result, expected);			
	}
	
	@Test(expectedExceptions = Exception.class)
	public void findByIdWithNotFoundTest() throws Exception{
			String maCd = "";

			ChuyenDe expected = null;
			List<ChuyenDe> resultList = new ArrayList<>();

			PowerMockito.doReturn(resultList).when(daoSpy, "select",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			ChuyenDe result = daoSpy.selectById(maCd);

			Assert.assertEquals(result, expected);
	}
}
