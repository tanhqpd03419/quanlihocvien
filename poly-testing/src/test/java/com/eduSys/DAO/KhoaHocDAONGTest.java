/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAO;

import Enity.KhoaHoc;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import static org.testng.Assert.*;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */
@PrepareForTest({JdbcHelper.class, KhoaHocDAO.class})
public class KhoaHocDAONGTest {

    KhoaHocDAO khoaHocDAO;
    KhoaHocDAO khoaHocDAOSpy;

    public KhoaHocDAONGTest() {
    }

    @ObjectFactory
    public IObjectFactory getIObjectFactory() {
        return new org.powermock.modules.testng.PowerMockObjectFactory();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        khoaHocDAO = new KhoaHocDAO();
        PowerMockito.mockStatic(JdbcHelper.class);
        khoaHocDAOSpy = PowerMockito.spy(new KhoaHocDAO());
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test(expectedExceptions = Exception.class)
    public void testInsertWithNoEnity() {
        //System.out.println("insert");
        KhoaHoc enity = null;
        KhoaHocDAO instance = new KhoaHocDAO();
        instance.insert(enity);

    }

    @Test
    public void testInsertWithEnity() {
        //System.out.println("insert");
        KhoaHoc enity = new KhoaHoc();
        KhoaHocDAO instance = new KhoaHocDAO();
        instance.insert(enity);

    }

    @Test
    public void testInsertWithValidEnity() {
        //System.out.println("insert");
        KhoaHoc enity = new KhoaHoc();
        enity.setGhiChu("");
        enity.setHocPhi(200);
        enity.setMaCD("");
        enity.setMaKH(1);
        enity.setMaNV("N01");
        enity.setNgayKG(new Date());
        enity.setNgayTao(new Date());
        enity.setThoiLuong(20);
        KhoaHocDAO instance = new KhoaHocDAO();
        instance.insert(enity);
    }

    @Test
    public void testUpdateWithnullEnity() {
        //System.out.println("update");
        KhoaHoc enity = null;
        KhoaHocDAO instance = new KhoaHocDAO();
        instance.update(enity);

    }

    @Test(expectedExceptions = Exception.class)
    public void testUpdateWithEnity() throws Exception {
        //System.out.println("update");
        KhoaHoc enity = new KhoaHoc();
        
//        PowerMockito.when(JdbcHelper.update(ArgumentMatchers.anyString(),
//				ArgumentMatchers.any(),ArgumentMatchers.any(),
//                                ArgumentMatchers.any(),ArgumentMatchers.any(),
//                                ArgumentMatchers.any(),ArgumentMatchers.any(),
//                                ArgumentMatchers.any()));
	PowerMockito.doThrow(new Exception()).
                when(JdbcHelper.class, "update", ArgumentMatchers.anyString(),
				ArgumentMatchers.any(),ArgumentMatchers.any(),
                                ArgumentMatchers.any(),ArgumentMatchers.any(),
                                ArgumentMatchers.any(),ArgumentMatchers.any(),
                                ArgumentMatchers.any());			
        
        khoaHocDAOSpy.update(enity);

    }

    @Test
    public void testUpdateWithvalidEnity() {
        //System.out.println("update");
        KhoaHoc enity = new KhoaHoc();
        enity.setGhiChu("");
        enity.setHocPhi(200);
        enity.setMaCD("");
        enity.setMaKH(1);
        enity.setMaNV("N01");
        enity.setNgayKG(new Date());
        enity.setNgayTao(new Date());
        enity.setThoiLuong(20);
        KhoaHocDAO instance = new KhoaHocDAO();
        instance.update(enity);

    }

    @Test
    public void testDeleteWithEnity() {
        //System.out.println("delete");
        String makh = "";

        khoaHocDAO.delete(makh);

    }

    @Test
    public void testDeleteWithNullEnity() {
        //System.out.println("delete");
        String makh = null;

        khoaHocDAO.delete(makh);

    }

    @Test
    public void testDeleteWithVadilEnity() {
        //System.out.println("delete");
        String makh = "11";

        khoaHocDAO.delete(makh);

    }

    @Test
    public void testSelectAll() {
        System.out.println("selectAll");
        KhoaHocDAO instance = new KhoaHocDAO();
        List expResult = null;
        List result = instance.selectAll();
        // AssertEquals(result, expResult);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSelectByIdWithValidID() throws Exception {
        //System.out.println("selectById");
        String makh = "12";
        KhoaHoc expectedResult = new KhoaHoc();
        List<KhoaHoc> resultList = new ArrayList<>();
        resultList.add(expectedResult);
        PowerMockito.doReturn(resultList).
                when(khoaHocDAOSpy, "selectBySql", ArgumentMatchers.anyString(), ArgumentMatchers.any());
        KhoaHoc result = khoaHocDAOSpy.selectById(makh);
        Assert.assertThat(result, CoreMatchers.is(expectedResult));

    }

    @Test
    public void testSelectByIdWithNotFoundID() throws Exception {
        //System.out.println("selectById");
        String makh = "";
        KhoaHoc expectedResult = null;
        List<KhoaHoc> resultList = new ArrayList<>();
        //resultList.add(expectedResult);
        PowerMockito.doReturn(resultList).
                when(khoaHocDAOSpy, "selectBySql", ArgumentMatchers.anyString(), ArgumentMatchers.any());

        KhoaHoc result = khoaHocDAOSpy.selectById(makh);
        Assert.assertThat(result, CoreMatchers.is(expectedResult));

    }

    @Test
    public void testSelectBySql() {
        System.out.println("selectBySql");
        String sql = "";
        Object[] args = null;
        KhoaHocDAO instance = new KhoaHocDAO();
        List expResult = null;
        List result = instance.selectBySql(sql, args);
        assertEquals(result, expResult);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSelectByChuyenDe() {
        System.out.println("selectByChuyenDe");
        String macd = "";
        KhoaHocDAO instance = new KhoaHocDAO();
        List expResult = null;
        List result = instance.selectByChuyenDe(macd);
        assertEquals(result, expResult);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSelectYears() {
        System.out.println("selectYears");
        KhoaHocDAO instance = new KhoaHocDAO();
        List expResult = null;
        List result = instance.selectYears();
        assertEquals(result, expResult);
        fail("The test case is a prototype.");
    }

}
	
       
//     KhoaHocDAO dao;
//	KhoaHocDAO daoSpy;
//
//	@ObjectFactory
//	public IObjectFactory getObjectFactory() {
//		return new org.powermock.modules.testng.PowerMockObjectFactory();
//	}
//
//	@BeforeMethod
//	public void beforeMethod() {
//		dao = new KhoaHocDAO();
//		PowerMockito.mockStatic(JdbcHelper.class);
//		daoSpy = PowerMockito.spy(new KhoaHocDAO());
//	}
//
//	@AfterMethod
//	public void afterMethod() {
//		dao = null;
//		daoSpy = null;
//	}
//
//	@BeforeTest
//	public void beforeTest() {
//	}
//
//	@AfterTest
//	public void afterTest() {
//	}
//
//	@Test(expectedExceptions = Exception.class)
//	public void deleteWithMaKhNullTest() {
//		String maKH = null;
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.delete(maKH);
//	}
//
//	@Test
//	public void deleteWithMaKhValidTest() {
//		String maKH = "01";
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.delete(maKH);
//	}
//
//	@Test(expectedExceptions = Exception.class)
//	public void insertWithNullModelTest() {
//		KhoaHoc model = null;
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.insert(model);
//	}
//
//	@Test(expectedExceptions = Exception.class)
//	public void insertWithEmptyModelTest() {
//		KhoaHoc model = new KhoaHoc();
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.insert(model);
//	}
//
//	@Test
//	public void insertWithValidModelTest() {
//		KhoaHoc model = new KhoaHoc();
//		model.setGhiChu("testng");
//		model.setHocPhi(500000);
//		model.setMaCD("CD001");
//		model.setMaKH(11);
//		model.setMaNV("NV001");
//		model.setNgayKG(new Date());
//		model.setNgayTao(new Date());
//		model.setThoiLuong(3);
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.insert(model);
//	}
//
//	@Test
//	public void selectTest() throws Exception {
//			List<KhoaHoc> expecteds = new ArrayList<>();
//			PowerMockito.doReturn(expecteds).when(daoSpy, "select", ArgumentMatchers.anyString(),
//					ArgumentMatchers.any());
//			List<KhoaHoc> actuals = daoSpy.selectAll();
//
//			org.testng.Assert.assertEquals(expecteds, actuals);
//
//	}
//
//	@Test(expectedExceptions = Exception.class)
//	public void updateWithNullModelTest() {
//		KhoaHoc model = null;
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.update(model);
//	}
//
//	@Test(expectedExceptions = Exception.class)
//	public void updateWithEmptyModelTest() {
//		KhoaHoc model = new KhoaHoc();
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.update(model);
//	}
//
//	@Test
//	public void updateWithValidModelTest() {
//		KhoaHoc model = new KhoaHoc();
//		model.setGhiChu("testng");
//		model.setHocPhi(300000);
//		model.setMaCD("CD001");
//		model.setMaKH(11);
//		model.setMaNV("NV001");
//		model.setNgayKG(new Date());
//		model.setNgayTao(new Date());
//		model.setThoiLuong(5);
//		KhoaHocDAO instance = new KhoaHocDAO();
//		instance.update(model);
//	}
//
//	@Test
//	public void findByIdWithValidModelTest() throws Exception {
//			String maKH = "11";
//
//			KhoaHoc expected = new KhoaHoc();
//			List<KhoaHoc> resultList = new ArrayList<>();
//			resultList.add(expected);
//
//			PowerMockito.doReturn(resultList).when(daoSpy, "select", ArgumentMatchers.anyString(),
//					ArgumentMatchers.any());
//
//			KhoaHoc result = daoSpy.selectById(maKH);
//
//			org.testng.Assert.assertEquals(result, expected);
//
//	}
//	
//	@Test
//	public void findByIdWithNontFoundTest() throws Exception {
//			String maKh = "11";
//
//			KhoaHoc expected = null;
//			List<KhoaHoc> resultList = new ArrayList<>();
//
//			PowerMockito.doReturn(resultList).when(daoSpy, "select", ArgumentMatchers.anyString(),
//					ArgumentMatchers.any());
//
//			KhoaHoc result = daoSpy.selectById(maKh);
//
//			org.testng.Assert.assertEquals(result, expected);
//
//	}
//}
