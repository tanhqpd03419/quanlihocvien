/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAOTest.junit;

import com.eduSys.DAO.*;
import com.eduSys.utils.JdbcHelper;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import com.mockrunner.mock.jdbc.MockStatement;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Administrator
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({JdbcHelper.class ,ThongKeDAO.class})
public class ThongKeDAOTest {
    ThongKeDAO thongKeDAOSpy;
    @Mock
    MockConnection connection;
    @Mock
    MockStatement statement;
    @Spy
    @InjectMocks
    MockResultSet rs = new MockResultSet("myMock");
    
    public ThongKeDAOTest() {
    }
    
    @Before
    public void setUp() {
        PowerMockito.mockStatic(JdbcHelper.class);
        thongKeDAOSpy =PowerMockito.spy(new ThongKeDAO());
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetBangDiem() {
    }

    @Test
    public void testGetLuongNguoiHoc()throws Exception{
        System.out.println("GetLuongNguoiHoc");
        rs =initMockResultSet();
        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
        PowerMockito.when(rs.getStatement()).thenReturn(statement);
        PowerMockito.when(statement.getConnection()).thenReturn(connection);
        List result = thongKeDAOSpy.getLuongNguoiHoc();
        assertEquals(1, result.size());
        
    }
       @Test
    public void testGetLuongNguoiHocWithMultipleData()throws Exception{
        System.out.println("GetLuongNguoiHoc");
        rs =initMultipleDataMockResultSet();
        PowerMockito.when(JdbcHelper.query(ArgumentMatchers.anyString())).thenReturn(rs);
        PowerMockito.when(rs.getStatement()).thenReturn(statement);
        PowerMockito.when(statement.getConnection()).thenReturn(connection);
        List result = thongKeDAOSpy.getLuongNguoiHoc();
        assertEquals(3, result.size());
        
    }

    @Test
    public void testGetDiemChuyenDe() {
    }

    @Test
    public void testGetDoanhThu() {
    }

    private MockResultSet initMockResultSet() throws SQLException {
        rs.addColumn("Nam",new Integer[]{1});
        rs.addColumn("soluong", new Integer[]{1});
        rs.addColumn("Dautien", new java.sql.Date[] {
        new java.sql.Date(new java.util.Date().getTime())});
        rs.addColumn("cuoicung", new java.sql.Date[] {
        new java.sql.Date(new java.util.Date().getTime())});
        rs.beforeFirst();
    
        
        return rs;
    }

    private MockResultSet initMultipleDataMockResultSet() throws SQLException {
        rs.addColumn("Nam",new Integer[]{1,2,5});
        rs.addColumn("soluong", new Integer[]{1,5,7});
        rs.addColumn("Dautien", new java.sql.Date[] {
              new java.sql.Date(new java.util.Date().getTime()),
              new java.sql.Date(new java.util.Date().getTime()),
              new java.sql.Date(new java.util.Date().getTime())
            
        });
        rs.addColumn("cuoicung", new java.sql.Date[] {
        new java.sql.Date(new java.util.Date().getTime()),
            new java.sql.Date(new java.util.Date().getTime()),
            new java.sql.Date(new java.util.Date().getTime())
        });
        rs.beforeFirst();
    
        
        return rs;
    }
    
}
