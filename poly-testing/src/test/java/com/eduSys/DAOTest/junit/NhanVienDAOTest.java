/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAOTest.junit;

import Enity.Nhanvien;
import com.eduSys.DAO.NhanVienDAO;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Administrator
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ JdbcHelper.class, NhanVienDAO.class })
public class NhanVienDAOTest {
  
	NhanVienDAO dao;
	NhanVienDAO daoSpy;

	@Before
	public void beforeMethod() {
		dao = new NhanVienDAO();
		PowerMockito.mockStatic(JdbcHelper.class);
		daoSpy = PowerMockito.spy(new NhanVienDAO());
	}
	
	@After
	public void afterMethod() {
		dao = null;
		daoSpy = null;
	}

	@Test(expected = Exception.class)
	public void deleteWithMaCdNullTest() {
		String maCd = null;
		dao.delete(maCd);
	}

	@Test
	public void deleteWithMaKhValidTest() {
		String maCd = "CD003";
		dao.delete(maCd);
	}

	@Test(expected = Exception.class)
	public void insertWithNullModelTest() {
		Nhanvien model = null;
		dao.insert(model);
	}

	@Test(expected = Exception.class)
	public void insertWithEmptyModelTest() {
		Nhanvien model = new Nhanvien();
		dao.insert(model);
	}

	@Test
	public void insertWithValidModelTest() {
		Nhanvien model = new Nhanvien();
		model.setHoten("Nguyen Van Muoi");
		model.setMaNV("NV010");
		model.setMatkhau("123");
		model.setVaiTro(false);
		dao.insert(model);
	}

	@Test(expected = Exception.class)
	public void selectTest() throws Exception{
			List<Nhanvien> expecteds = new ArrayList<>();
			PowerMockito.doReturn(expecteds).when(daoSpy, "selectAll", ArgumentMatchers.anyString(),
					ArgumentMatchers.any());
			List<Nhanvien> actuals = daoSpy.selectAll();

			Assert.assertEquals(expecteds, actuals);
	}

	@Test(expected = Exception.class)
	public void updateWithNullModelTest() {
		Nhanvien model = null;
		dao.update(model);
	}

	@Test(expected = Exception.class)
	public void updateWithEmptyModelTest() {
		Nhanvien model = new Nhanvien();
		dao.update(model);
	}

	@Test
	public void updateWithValidModelTest() {
		Nhanvien model = new Nhanvien();
		model.setHoten("Nguyen Van Muoi");
		model.setMaNV("NV010");
		model.setMatkhau("123");
		model.setVaiTro(false);
		dao.update(model);
	}

	@Test(expected = Exception.class)
	public void findByIdWithValidModelTest() throws Exception{
			String maNv = "NV001";
			
			Nhanvien expected = new Nhanvien();
			List<Nhanvien> resultList = new ArrayList<>();
			resultList.add(expected);

			PowerMockito.doReturn(resultList).when(daoSpy, "selectById",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			Nhanvien result = daoSpy.selectById(maNv);
			
			System.out.println("--- ^^^ ++++ " + result);
			System.out.println("--- ^^^ ++++ " + expected);
			
			Assert.assertThat(result, CoreMatchers.is(expected));			
	}
	
	@Test(expected = Exception.class)
	public void findByIdWithNotFoundTest() throws Exception{
			String maNv = "NV111";

			Nhanvien expected = null;
			List<Nhanvien> resultList = new ArrayList<>();

			PowerMockito.doReturn(resultList).when(daoSpy, "selectById",
					ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			Nhanvien result = daoSpy.selectById(maNv);

			Assert.assertThat(result, CoreMatchers.is(expected));
	}
}
