/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAOTest.junit;

import com.eduSys.DAO.*;
import Enity.ChuyenDe;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Administrator
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({JdbcHelper.class , ChuyenDeDAO.class})
public class ChuyenDeDAOTest {
     ChuyenDeDAO chuyenDeDAO;
     
      ChuyenDeDAO chuyenDeDaoSpy;
    public ChuyenDeDAOTest() {
     
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
     chuyenDeDAO =new ChuyenDeDAO();
        PowerMockito.mockStatic(JdbcHelper.class);
         chuyenDeDaoSpy =PowerMockito.spy(new ChuyenDeDAO());
    }
    
    @After
    public void tearDown() {
    }

    @Test(expected = Exception.class)
    public void testInsertWithNullModel() {
        //System.out.println("insert");
        ChuyenDe enity = null;
       
       chuyenDeDAO.insert(enity);
        
    }
     @Test(expected = Exception.class)
    public void testInsertWithEmptyModel() {
        //System.out.println("insert");
        ChuyenDe enity = new ChuyenDe();
       
       chuyenDeDAO.insert(enity);
        
    }
 @Test()
    public void testInsertWithValiModel() {
        //System.out.println("insert");
        ChuyenDe enity = new ChuyenDe();
        enity.setHinh("test.jpg");
        enity.setHocPhi(100);
        enity.setMaCD("CD01");
        enity.setMoTa("Mo ta");
        enity.setThoiLuong(20);
        enity.setTenCD("chuyen de");
       chuyenDeDAO.insert(enity);
        
    }
    @Test(expected = Exception.class)
    public void testUpdateWithnullEnity() {
        //System.out.println("update");
        ChuyenDe enity = null;
        chuyenDeDAO.update(enity);
        
    }
     @Test(expected = Exception.class)
    public void testUpdateWithEmptylEnity() {
        //System.out.println("update");
        ChuyenDe enity = new ChuyenDe();
        chuyenDeDAO.update(enity);
        
    }
    @Test(expected = Exception.class)
    public void testUpdateWithValilEnity() {
        //System.out.println("update");
        ChuyenDe enity = new ChuyenDe();
        enity.setHinh("test.jpg");
        enity.setHocPhi(100);
        enity.setMaCD("CD01");
        enity.setMoTa("Mo ta");
        enity.setTenCD("chuyen de");
        enity.setThoiLuong(20);
        
        chuyenDeDAO.update(enity);
        
    }

    @Test(expected = Exception.class)
    public void testDeleteWithEmptyId() {
        //System.out.println("delete");
        String macd = "";
        
        chuyenDeDAO.delete(macd);
       
    }
    @Test(expected = Exception.class)
    public void testDeleteWithNullId() {
        //System.out.println("delete");
        String macd = null;
        
        chuyenDeDAO.delete(macd);
       
    }
     @Test(expected = Exception.class)
    public void testDeleteWithValiId() {
        //System.out.println("delete");
        String macd = "12";
        
        chuyenDeDAO.delete(macd);
       
    }

    @Test(expected = Exception.class)
    public void testSelectAll() throws Exception{
        //System.out.println("selectAll");
        ChuyenDeDAO instance = new ChuyenDeDAO();
        List<ChuyenDe> expResult = new ArrayList<>();
       PowerMockito.doReturn(expResult).when(chuyenDeDaoSpy,
               "selectAll",ArgumentMatchers.anyString(),
					ArgumentMatchers.any());
       List<ChuyenDe> result = chuyenDeDaoSpy.selectAll();
    assertEquals(result,CoreMatchers.is(expResult));
//        assertThat(result, CoreMatchers.is(expResult));
//List<ChuyenDe> expecteds = new ArrayList<>();
//			PowerMockito.doReturn(expecteds).when(chuyenDeDaoSpy, "selectAll",
//					ArgumentMatchers.anyString(),
//					ArgumentMatchers.any());
//			List<ChuyenDe> actuals = chuyenDeDaoSpy.selectAll();
//
//			Assert.assertEquals(expecteds, actuals);
        
    }

    @Test(expected = Exception.class)
    public void testSelectByIdWithNotFound() throws Exception {
       // System.out.println("selectById");
        String macd = "";
         ChuyenDe expResult = null;
        List<ChuyenDe> resultList = new ArrayList<>();
       PowerMockito.doReturn(expResult).
               when(chuyenDeDaoSpy, "SelectById",ArgumentMatchers.anyString(),ArgumentMatchers.any());
        ChuyenDe result = chuyenDeDaoSpy.selectById(macd);
       assertEquals(result, CoreMatchers.is(expResult));
    }
     @Test(expected = Exception.class)
    public void testSelectByIdWithFound() throws Exception {
        //System.out.println("selectById");
        String macd = "12";
         ChuyenDe expResult = new ChuyenDe();
        List<ChuyenDe> resultList = new ArrayList<>();
        resultList.add(expResult);
       PowerMockito.doReturn(expResult).
               when(chuyenDeDaoSpy, "selectAll"
                       ,ArgumentMatchers.anyString(),ArgumentMatchers.any());
        ChuyenDe result = chuyenDeDaoSpy.selectById(macd);
      assertEquals(result, CoreMatchers.is(expResult));
    }

    @Test
    public void testSelectBySql() {
       // System.out.println("selectBySql");
        String macd = "123";
        Object[] args = null;
        ChuyenDeDAO instance = new ChuyenDeDAO();
        List<ChuyenDe> expResult = null;
         ChuyenDe result = instance.selectById(macd);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
}
