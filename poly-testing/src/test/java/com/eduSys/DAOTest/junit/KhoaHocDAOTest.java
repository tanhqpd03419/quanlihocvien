/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eduSys.DAOTest.junit;

import Enity.KhoaHoc;
import com.eduSys.DAO.KhoaHocDAO;
import com.eduSys.utils.JdbcHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Administrator
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ JdbcHelper.class, KhoaHocDAO.class })
public class KhoaHocDAOTest {
    
   KhoaHocDAO dao;
	KhoaHocDAO daoSpy;


	@Before
	public void beforeMethod() {
		dao = new KhoaHocDAO();
		PowerMockito.mockStatic(JdbcHelper.class);
		daoSpy = PowerMockito.spy(new KhoaHocDAO());
	}

	@After
	public void afterMethod() {
		dao = null;
		daoSpy = null;
	}

	@Test(expected = Exception.class)
	public void deleteWithMaKhNullTest() {
		String maKH = null;
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.delete(maKH);
	}

	@Test
	public void deleteWithMaKhValidTest() {
		String maKH = "12";
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.delete(maKH);
	}

	@Test(expected = Exception.class)
	public void insertWithNullModelTest() {
		KhoaHoc model = null;
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.insert(model);
	}

	@Test(expected = Exception.class)
	public void insertWithEmptyModelTest() {
		KhoaHoc model = new KhoaHoc();
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.insert(model);
	}

	@Test
	public void insertWithValidModelTest() {
		KhoaHoc model = new KhoaHoc();
		model.setGhiChu("testng");
		model.setHocPhi(500000);
		model.setMaCD("CD001");
		model.setMaKH(11);
		model.setMaNV("NV001");
		model.setNgayKG(new Date());
		model.setNgayTao(new Date());
		model.setThoiLuong(3);
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.insert(model);
	}

	@Test(expected = Exception.class)
	public void selectTest() throws Exception {
			List<KhoaHoc> expecteds = new ArrayList<>();
			PowerMockito.doReturn(expecteds).when(daoSpy, "selectAll", ArgumentMatchers.anyString(),
					ArgumentMatchers.any());
			List<KhoaHoc> actuals = daoSpy.selectAll();

			Assert.assertEquals(expecteds, actuals);

	}

	@Test(expected = Exception.class)
	public void updateWithNullModelTest() {
		KhoaHoc model = null;
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.update(model);
	}

	@Test(expected = Exception.class)
	public void updateWithEmptyModelTest() {
		KhoaHoc model = new KhoaHoc();
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.update(model);
	}

	@Test
	public void updateWithValidModelTest() {
		KhoaHoc model = new KhoaHoc();
		model.setGhiChu("testng");
		model.setHocPhi(300000);
		model.setMaCD("CD001");
		model.setMaKH(11);
		model.setMaNV("NV001");
		model.setNgayKG(new Date());
		model.setNgayTao(new Date());
		model.setThoiLuong(5);
		KhoaHocDAO instance = new KhoaHocDAO();
		instance.update(model);
	}

	@Test(expected = Exception.class)
	public void findByIdWithValidModelTest() throws Exception {
			String maKh = "1";

			KhoaHoc expected = new KhoaHoc();
			List<KhoaHoc> resultList = new ArrayList<>();
			resultList.add(expected);

			PowerMockito.doReturn(resultList).when(daoSpy, "selectbyId", ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			KhoaHoc result = daoSpy.selectById(maKh);

			Assert.assertThat(result, CoreMatchers.is(expected));

	}
	
	@Test(expected = Exception.class)
	public void findByIdWithNontFoundTest() throws Exception {
			String maKH = "11";

			KhoaHoc expected = null;
			List<KhoaHoc> resultList = new ArrayList<>();

			PowerMockito.doReturn(resultList).when(daoSpy, "selectById", ArgumentMatchers.anyString(),
					ArgumentMatchers.any());

			KhoaHoc result = daoSpy.selectById(maKH);

			Assert.assertThat(result, CoreMatchers.is(expected));

	}
}
