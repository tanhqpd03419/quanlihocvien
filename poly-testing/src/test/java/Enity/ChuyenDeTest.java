/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class ChuyenDeTest {
    
    public ChuyenDeTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of toString method, of class ChuyenDe.
     */


    /**
     * Test of getMaCD method, of class ChuyenDe.
     */
    @Test
    public void testGetMaCD() {
        
        ChuyenDe instance = new ChuyenDe();
        String expResult = "";
        String result = instance.getMaCD();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaCD method, of class ChuyenDe.
     */
    @Test
    public void testSetMaCD() {
        
        String MaCD = "M01";
        ChuyenDe instance = new ChuyenDe();
        instance.setMaCD(MaCD);
        String expected ="M01";
        assertEquals(expected, instance.getMaCD());
    }

    /**
     * Test of getTenCD method, of class ChuyenDe.
     */
    @Test
    public void testGetTenCD() {
       
        ChuyenDe instance = new ChuyenDe();
        String expResult = "";
        String result = instance.getTenCD();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setTenCD method, of class ChuyenDe.
     */
    @Test
    public void testSetTenCD() {
    
        String TenCD = "java";
        ChuyenDe instance = new ChuyenDe();
        instance.setTenCD(TenCD);
        String expected ="java";
        assertEquals(expected, instance.getTenCD());
       
    }

    /**
     * Test of getHocPhi method, of class ChuyenDe.
     */
    @Test
    public void testGetHocPhi() {
       
        ChuyenDe instance = new ChuyenDe();
        float expResult = 0.0F;
        float result = instance.getHocPhi();
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of setHocPhi method, of class ChuyenDe.
     */
    @Test
    public void testSetHocPhi() {
       
        float HocPhi = 600;
        ChuyenDe instance = new ChuyenDe();
        instance.setHocPhi(HocPhi);
        float expected = 600;
        float result = instance.getHocPhi();
        assertEquals(expected,result,0.0);
    }
        @Test
    public void testSetHocPhiWithNegative() {
       
        float HocPhi = -600;
        ChuyenDe instance = new ChuyenDe();
      
        Exception exception = assertThrows(Exception.class,
                ()-> instance.setHocPhi(HocPhi));
       
    }
    @Test
    public void testSetHocPhiLarge() {
       
        float HocPhi = 600000000000000000000f;
        ChuyenDe instance = new ChuyenDe();
      
        Exception exception = assertThrows(Exception.class,
                ()-> instance.setHocPhi(HocPhi));
       
    }

    /**
     * Test of getThoiLuong method, of class ChuyenDe.
     */
    @Test
    public void testGetThoiLuong() {
       
        ChuyenDe instance = new ChuyenDe();
        int expResult = 0;
        int result = instance.getThoiLuong();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setThoiLuong method, of class ChuyenDe.
     */
    @Test
    public void testSetThoiLuong() {
     
        int ThoiLuong = 10;
        ChuyenDe instance = new ChuyenDe();
        instance.setThoiLuong(ThoiLuong);
      int expected =10;
      int result = instance.getThoiLuong();
      assertEquals(expected, result);
    }
    @Test
    public void testSetThoiLuongWithNegative() {
     
        int ThoiLuong = -10;
        ChuyenDe instance = new ChuyenDe();
        instance.setThoiLuong(ThoiLuong);
         Exception exception = assertThrows(Exception.class,
                ()-> instance.setThoiLuong(ThoiLuong));
    
    }

    /**
     * Test of getHinh method, of class ChuyenDe.
     */
    @Test
    public void testGetHinh() {
        
        ChuyenDe instance = new ChuyenDe();
        String expResult = "";
        String result = instance.getHinh();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setHinh method, of class ChuyenDe.
     */
    @Test
    public void testSetHinh() {
 
        String Hinh = "logo.png";
        ChuyenDe instance = new ChuyenDe();
        instance.setHinh(Hinh);
        String expected ="logo.png";
        assertEquals(expected, instance.getHinh());
    }

    /**
     * Test of getMoTa method, of class ChuyenDe.
     */
    @Test
    public void testGetMoTa() {
        
        ChuyenDe instance = new ChuyenDe();
        String expResult = "";
        String result = instance.getMoTa();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMoTa method, of class ChuyenDe.
     */
    @Test
    public void testSetMoTa() {
     
        String MoTa = "ok";
        ChuyenDe instance = new ChuyenDe();
        instance.setMoTa(MoTa);
        String expected ="ok";
        assertEquals(expected, instance.getMoTa());
    }
    
}
