/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class HocVienTest {
    
    public HocVienTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of toString method, of class HocVien.
     */

    /**
     * Test of getMaHV method, of class HocVien.
     */
    @Test
    public void testGetMaHV() {
     ;
        HocVien instance = new HocVien();
        int expResult = 0;
        int result = instance.getMaHV();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaHV method, of class HocVien.
     */
    @Test
    public void testSetMaHV() {
        
        int MaHV = 1;
        HocVien instance = new HocVien();
        instance.setMaHV(MaHV);
         int expected = 1;
        assertEquals(expected, instance.getMaHV());
    }

    /**
     * Test of getMaKH method, of class HocVien.
     */
    @Test
    public void testGetMaKH() {
        
        HocVien instance = new HocVien();
        int expResult = 0;
        int result = instance.getMaKH();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMaKH method, of class HocVien.
     */
    @Test
    public void testSetMaKH() {
       
        int MaKH = 2;
        HocVien instance = new HocVien();
        instance.setMaKH(MaKH);
        int expected = 2;
        assertEquals(expected, instance.getMaKH());
    }

    /**
     * Test of getMaNH method, of class HocVien.
     */
    @Test
    public void testGetMaNH() {

        HocVien instance = new HocVien();
        String expResult = "";
        String result = instance.getMaNH();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaNH method, of class HocVien.
     */
    @Test
    public void testSetMaNH() {
        
        String MaNH = "M01";
        HocVien instance = new HocVien();
        instance.setMaNH(MaNH);
          String expected ="M01";
        assertEquals(expected, instance.getMaNH());
    }

    /**
     * Test of getDiem method, of class HocVien.
     */
    @Test
    public void testGetDiem() {
        
        HocVien instance = new HocVien();
        float expResult = 0.0F;
        float result = instance.getDiem();
        assertEquals(expResult, result, 0.0);
   
    }

    /**
     * Test of setDiem method, of class HocVien.
     */
    @Test
    public void testSetDiem() {
        
        float Diem = 10;
        HocVien instance = new HocVien();
        instance.setDiem(Diem);
        float expected = 10;
        float result = instance.getDiem();
        assertEquals(expected,result,0.0);
    }
    @Test
    public void testSetDiemWithNegative() {
       
        float Diem = -10;
        HocVien instance = new HocVien();
      
        Exception exception = assertThrows(Exception.class,
                ()-> instance.setDiem(Diem));
       
    }
    
}
