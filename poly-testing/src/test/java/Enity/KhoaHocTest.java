/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class KhoaHocTest {
    
    public KhoaHocTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of toString method, of class KhoaHoc.
     */
   

    /**
     * Test of getMaKH method, of class KhoaHoc.
     */
    @Test
    public void testGetMaKH() {
       
        KhoaHoc instance = new KhoaHoc();
        int expResult = 0;
        int result = instance.getMaKH();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaKH method, of class KhoaHoc.
     */
    @Test
    public void testSetMaKH() {
       // System.out.println("setMaKH");
        int MaKH = 0;
        KhoaHoc instance = new KhoaHoc();
        instance.setMaKH(MaKH);
          int expected = 0;
          
        assertEquals(expected, instance.getMaKH());
     
    }

    /**
     * Test of getMaCD method, of class KhoaHoc.
     */
    @Test
    public void testGetMaCD() {
        //System.out.println("getMaCD");
        KhoaHoc instance = new KhoaHoc();
        String expResult = "";
        String result = instance.getMaCD();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaCD method, of class KhoaHoc.
     */
    @Test
    public void testSetMaCD() {
        //System.out.println("setMaCD");
        String MaCD = "M01";
        KhoaHoc instance = new KhoaHoc();
        instance.setMaCD(MaCD);
          String expected ="M01";
        assertEquals(expected, instance.getMaCD());
       
    }

    /**
     * Test of getHocPhi method, of class KhoaHoc.
     */
    @Test
    public void testGetHocPhi() {
        //System.out.println("getHocPhi");
        KhoaHoc instance = new KhoaHoc();
        float expResult = 0.0F;
        float result = instance.getHocPhi();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setHocPhi method, of class KhoaHoc.
     */
    @Test
    public void testSetHocPhi() {
        //System.out.println("setHocPhi");
        float HocPhi = 600;
        KhoaHoc instance = new KhoaHoc();
        instance.setHocPhi(HocPhi);
        float expected = 600;
        float result = instance.getHocPhi();
        assertEquals(expected,result,0.0);
        
    }
     @Test
    public void testSetHocPhiWithNegative() {
       
        float HocPhi = -600;
        KhoaHoc instance = new KhoaHoc();
      
        Exception exception = assertThrows(Exception.class,
                ()-> instance.setHocPhi(HocPhi));
       
    }
    @Test
    public void testSetHocPhiLarge() {
       
        float HocPhi = 600000000000000000000f;
        KhoaHoc instance = new KhoaHoc();
      
        Exception exception = assertThrows(Exception.class,
                ()-> instance.setHocPhi(HocPhi));
       
    }


    /**
     * Test of getThoiLuong method, of class KhoaHoc.
     */
    @Test
    public void testGetThoiLuong() {
        //System.out.println("getThoiLuong");
        KhoaHoc instance = new KhoaHoc();
        int expResult = 0;
        int result = instance.getThoiLuong();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setThoiLuong method, of class KhoaHoc.
     */
    @Test
    public void testSetThoiLuong() {
        //System.out.println("setThoiLuong");
        int ThoiLuong = 3;
        KhoaHoc instance = new KhoaHoc();
        instance.setThoiLuong(ThoiLuong);
         int expected =3;
        int result = instance.getThoiLuong();
       assertEquals(expected, result);
        
    }
        @Test
    public void testSetThoiLuongWithNegative() {
     
        int ThoiLuong = -10;
        KhoaHoc instance = new KhoaHoc();
        instance.setThoiLuong(ThoiLuong);
         Exception exception = assertThrows(Exception.class,
                ()-> instance.setThoiLuong(ThoiLuong));
    
    }

    /**
     * Test of getNgayKG method, of class KhoaHoc.
     */
    @Test
    public void testGetNgayKG() {
        
        KhoaHoc instance = new KhoaHoc();
        Date expResult = null;
        Date result = instance.getNgayKG();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setNgayKG method, of class KhoaHoc.
     */
    @Test
    public void testSetNgayKG() {
       
        Date NgayKG = null;
        KhoaHoc instance = new KhoaHoc();
        instance.setNgayKG(NgayKG);
        Date expected =null;
      Date result = instance.getNgayKG();
      assertEquals(expected, result);
     
        
        
    }

    /**
     * Test of getGhiChu method, of class KhoaHoc.
     */
    @Test
    public void testGetGhiChu() {
       // System.out.println("getGhiChu");
        KhoaHoc instance = new KhoaHoc();
        String expResult = "";
        String result = instance.getGhiChu();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setGhiChu method, of class KhoaHoc.
     */
    @Test
    public void testSetGhiChu() {
       // System.out.println("setGhiChu");
        String GhiChu = "ok";
        KhoaHoc instance = new KhoaHoc();
        instance.setGhiChu(GhiChu);
        String  expected = "ok";
      String result = instance.getGhiChu();
      assertEquals(expected, result);
       
    }

    /**
     * Test of getMaNV method, of class KhoaHoc.
     */
    @Test
    public void testGetMaNV() {
       // System.out.println("getMaNV");
        KhoaHoc instance = new KhoaHoc();
        String expResult = "";
        String result = instance.getMaNV();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaNV method, of class KhoaHoc.
     */
    @Test
    public void testSetMaNV() {
        //System.out.println("setMaNV");
        String MaNV = "M02";
        KhoaHoc instance = new KhoaHoc();
        instance.setMaNV(MaNV);
       String  expected ="M02";
      String result = instance.getMaNV();
      assertEquals(expected, result);
    }

    /**
     * Test of getNgayTao method, of class KhoaHoc.
     */
    @Test
    public void testGetNgayTao() {
        //System.out.println("getNgayTao");
        KhoaHoc instance = new KhoaHoc();
        Date expResult = null;
        Date result = instance.getNgayTao();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setNgayTao method, of class KhoaHoc.
     */
    @Test
    public void testSetNgayTao() {
        System.out.println("setNgayTao");
        Date NgayTao = null;
        KhoaHoc instance = new KhoaHoc();
        instance.setNgayTao(NgayTao);
        Date expected =null;
      Date result = instance.getNgayTao();
      assertEquals(expected, result);
    }

    /**
     * Test of getNguoiTao method, of class KhoaHoc.
     */
    @Test
    public void testGetNguoiTao() {
       // System.out.println("getNguoiTao");
        KhoaHoc instance = new KhoaHoc();
        String expResult = "";
        String result = instance.getNguoiTao();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setNguoiTao method, of class KhoaHoc.
     */
    @Test
    public void testSetNguoiTao() {
        System.out.println("setNguoiTao");
        String NguoiTao = "T01";
        KhoaHoc instance = new KhoaHoc();
        instance.setNguoiTao(NguoiTao);
        String expected ="T01";
      String result = instance.getNguoiTao();
      assertEquals(expected, result);
    }
    
}
