/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class NhanvienTest {
    
    public NhanvienTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getMaNV method, of class Nhanvien.
     */
    @Test
    public void testGetMaNV() {
       // System.out.println("getMaNV");
        Nhanvien instance = new Nhanvien();
        String expResult = "";
        String result = instance.getMaNV();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaNV method, of class Nhanvien.
     */
    @Test
    public void testSetMaNV() {
        //System.out.println("setMaNV");
        String MaNV = "M1";
        Nhanvien instance = new Nhanvien();
        instance.setMaNV(MaNV);
        String expected ="M1";
        String result = instance.getMaNV();
      assertEquals(expected, result);
    }

    /**
     * Test of getMatkhau method, of class Nhanvien.
     */
    @Test
    public void testGetMatkhau() {
        //System.out.println("getMatkhau");
        Nhanvien instance = new Nhanvien();
        String expResult = "";
        String result = instance.getMatkhau();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMatkhau method, of class Nhanvien.
     */
    @Test
    public void testSetMatkhau() {
        //System.out.println("setMatkhau");
        String Matkhau = "abc";
        Nhanvien instance = new Nhanvien();
        instance.setMatkhau(Matkhau);
        String expected ="abc";
       String result = instance.getMatkhau();
      assertEquals(expected, result);
    }

    /**
     * Test of isVaiTro method, of class Nhanvien.
     */
    @Test
    public void testIsVaiTro() {
        //System.out.println("isVaiTro");
        Nhanvien instance = new Nhanvien();
        boolean expResult = false;
        boolean result = instance.isVaiTro();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setVaiTro method, of class Nhanvien.
     */
    @Test
    public void testSetVaiTro() {
        
        boolean VaiTro = false;
        Nhanvien instance = new Nhanvien();
        instance.setVaiTro(VaiTro);
       boolean expected =false;
      boolean result = instance.isVaiTro();
      assertEquals(expected, result);
    }

    /**
     * Test of getHoten method, of class Nhanvien.
     */
    @Test
    public void testGetHoten() {
       // System.out.println("getHoten");
        Nhanvien instance = new Nhanvien();
        String expResult = "";
        String result = instance.getHoten();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setHoten method, of class Nhanvien.
     */
    @Test
    public void testSetHoten() {
        //System.out.println("setHoten");
        String hoten = "tan";
        Nhanvien instance = new Nhanvien();
        instance.setHoten(hoten);
        String expected ="tan";
        String result = instance.getHoten();
      assertEquals(expected, result);
    }
    
}
