/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enity;

import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class NguoiHocTest {
    
    public NguoiHocTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getMaNH method, of class NguoiHoc.
     */
    @Test
    public void testGetMaNH() {
        //System.out.println("getMaNH");
        NguoiHoc instance = new NguoiHoc();
        String expResult = "";
        String result = instance.getMaNH();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaNH method, of class NguoiHoc.
     */
    @Test
    public void testSetMaNH() {
        //System.out.println("setMaNH");
        String MaNH = "M03";
        NguoiHoc instance = new NguoiHoc();
        instance.setMaNH(MaNH);
       String expected ="M03";
      String result = instance.getMaNH();
      assertEquals(expected, result);
    }

    /**
     * Test of getHoTen method, of class NguoiHoc.
     */
    @Test
    public void testGetHoTen() {
        //System.out.println("getHoTen");
        NguoiHoc instance = new NguoiHoc();
        String expResult = "";
        String result = instance.getHoTen();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of setHoTen method, of class NguoiHoc.
     */
    @Test
    public void testSetHoTen() {
        //System.out.println("setHoTen");
        String HoTen = "tan";
        NguoiHoc instance = new NguoiHoc();
        instance.setHoTen(HoTen);
       String expected ="tan";
      String result = instance.getHoTen();
      assertEquals(expected, result);
    }

    /**
     * Test of getNgaySinh method, of class NguoiHoc.
     */
    @Test
    public void testGetNgaySinh() {
        //System.out.println("getNgaySinh");
        NguoiHoc instance = new NguoiHoc();
        Date expResult = null;
        Date result = instance.getNgaySinh();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setNgaySinh method, of class NguoiHoc.
     */
    @Test
    public void testSetNgaySinh() {
        //System.out.println("setNgaySinh");
        Date NgaySinh = null;
        NguoiHoc instance = new NguoiHoc();
        instance.setNgaySinh(NgaySinh);
       Date expected =null;
      Date result = instance.getNgaySinh();
      assertEquals(expected, result);
    }

    /**
     * Test of isGioiTinh method, of class NguoiHoc.
     */
    @Test
    public void testIsGioiTinh() {
        System.out.println("isGioiTinh");
        NguoiHoc instance = new NguoiHoc();
        boolean expResult = false;
        boolean result = instance.isGioiTinh();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setGioiTinh method, of class NguoiHoc.
     */
    @Test
    public void testSetGioiTinh() {
        //System.out.println("setGioiTinh");
        boolean GioiTinh = false;
        NguoiHoc instance = new NguoiHoc();
        instance.setGioiTinh(GioiTinh);
        boolean expected =false;
      boolean result = instance.isGioiTinh();
      assertEquals(expected, result);
    }

    /**
     * Test of getDienThoai method, of class NguoiHoc.
     */
    @Test
    public void testGetDienThoai() {
        //System.out.println("getDienThoai");
        NguoiHoc instance = new NguoiHoc();
        int expResult = 0;
        int result = instance.getDienThoai();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setDienThoai method, of class NguoiHoc.
     */
    @Test
    public void testSetDienThoai() {
        //System.out.println("setDienThoai");
        int DienThoai = 0236;
        NguoiHoc instance = new NguoiHoc();
        instance.setDienThoai(DienThoai);
        int expected =0236;
      int result = instance.getDienThoai();
      assertEquals(expected, result);
    }

    /**
     * Test of getEmail method, of class NguoiHoc.
     */
    @Test
    public void testGetEmail() {
        //System.out.println("getEmail");
        NguoiHoc instance = new NguoiHoc();
        String expResult = "";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmail method, of class NguoiHoc.
     */
    @Test
    public void testSetEmail() {
        //System.out.println("setEmail");
        String Email = "tan@gmail.com";
        NguoiHoc instance = new NguoiHoc();
        instance.setEmail(Email);
       String expected ="tan@gmail.com";
      String result = instance.getEmail();
      assertEquals(expected, result);
    }

    /**
     * Test of getGhiChu method, of class NguoiHoc.
     */
    @Test
    public void testGetGhiChu() {
        //System.out.println("getGhiChu");
        NguoiHoc instance = new NguoiHoc();
        String expResult = "";
        String result = instance.getGhiChu();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setGhiChu method, of class NguoiHoc.
     */
    @Test
    public void testSetGhiChu() {
        //System.out.println("setGhiChu");
        String GhiChu = "ok";
        NguoiHoc instance = new NguoiHoc();
        instance.setGhiChu(GhiChu);
        String expected ="ok";
       String  result = instance.getGhiChu();
      assertEquals(expected, result);
    }

    /**
     * Test of getMaNV method, of class NguoiHoc.
     */
    @Test
    public void testGetMaNV() {
        //System.out.println("getMaNV");
        NguoiHoc instance = new NguoiHoc();
        String expResult = "";
        String result = instance.getMaNV();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaNV method, of class NguoiHoc.
     */
    @Test
    public void testSetMaNV() {
        //System.out.println("setMaNV");
        String MaNV = "23";
        NguoiHoc instance = new NguoiHoc();
        instance.setMaNV(MaNV);
        String expected ="23";
       String result = instance.getMaNV();
      assertEquals(expected, result);
    }

    /**
     * Test of getNgayDK method, of class NguoiHoc.
     */
    @Test
    public void testGetNgayDK() {
        //System.out.println("getNgayDK");
        NguoiHoc instance = new NguoiHoc();
        Date expResult = null;
        Date result = instance.getNgayDK();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setNgayDK method, of class NguoiHoc.
     */
    @Test
    public void testSetNgayDK() {
        //System.out.println("setNgayDK");
        Date NgayDK = null;
        NguoiHoc instance = new NguoiHoc();
        instance.setNgayDK(NgayDK);
        Date expected =null;
        Date result = instance.getNgayDK();
        assertEquals(expected, result);
    }
    
}
